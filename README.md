# README #

This is a programming language for rendering models into templates. It is heavily inspired by the play2framework and razor.

Key Features
------------

# Design-Time Compiled Views
# Ability to linking from Controller, without using strings to lookup the view.
# Ability to unit test views.
# Intuitive API on views
# Uses F# as its data manipulation engine, so you don't have to learn anything new.
# Templates can take multiple parameters
# Your view after compilation will have no dependencies other than the ones you and and the fSharp.core libary
# You can run views on any system. Not Just the web!


Anatomy of a template
-------------------

FileName
========

Each filename should follow the pattern: {ClassName}.{backingLanguageExtension}.{outputFormatExtension}.

Currently supported output formats and extension:
Html -> ".html"
Txt  -> ".txt"
Xml  -> ".xml"
Javascript -> ".js"

Examples:

`Index.fs.html`
Means that the output will be a class called `Index` and the code inside of the processing directives
 ( `@{}` ) are to be treated as f# (the only currently supported language), and the template will output html.
 
 
`AboutUs.fs.xml`
Means that the output will be a class called `AboutUs` and the code inside of the processing directives
 ( `@{}` ) are to be treated as f#, and the template will output xml.

### Adding new format types
You can add more format types by creating a class that implements IAppendable and has a namespace of `Base2art.MonkeyTail.ContentTypes`.
  Place your assembly in the bin of the project, and passing the flags --outputFormats={ClassName}:{fileExtension}
  This may get easier or change in future versions 

Header (Optional)
=================

Defines the type of parameters the template takes.

Each Parameter must go on its own line.
syntax is {At-symbol}{parameterName}{colon}{typeNameAsTypedIn_F#}

Example:
```
@title: string
@count: int
```

Tells the template that there are two parameters one is a string (the `title`) and another is an int (the `count`).



Body Start Marker
=================

All templates must contain an indicator of when parameters stop and when the template parser should start. 
The marker is:

`@-`

Example:
```
@title: string
@count: int
@-

@* tells the processor that there are 2 parameters then the template body starts. *@
```



Example:
```
@-

@* tells the processor that there are 0 parameters then the template body starts. *@
```



Template Bodies
===============

There are very few control flows in MonkeyTail (the processor), almost all of them are simply hooks to 
call F# methods and return values.  Because the templates work with F# all methods must return a value,
in other words *expressions only*, and *no statements*

### Examples

#### Empty Template

```

@-

```

Outputs

``


#### Template printing encoded value simple

```
@count:int
@-

@count

```

Outputs

`0`


#### Template printing encoded value complex 

```
@-

@{ System.DateTime.UtcNow }

```

Outputs

`4/23/2016 7:56:53 PM`


#### Html Template printing encoded value

```

@-

@{ "<br />" }

```

Outputs

`&amp;lt;br /&amp;gt;`



#### Html Template printing raw value

```

@-

@< "<br />" >@

```

Outputs

`&lt;br /&gt;`



#### Template With Comment

```

@-

@{ System.DateTime.UtcNow }

```

Outputs

`4/23/2016 7:56:53 PM`
