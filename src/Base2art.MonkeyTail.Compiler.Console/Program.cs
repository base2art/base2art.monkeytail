// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    using System;
    using System.Linq;
    using Base2art.MonkeyTail.Config;
    using Base2art.MonkeyTail.Diagnostics;
    using CommandLine;

    public class Program
    {
        private static int Main(string[] args)
        {
            return CommandLine.Parser.Default.ParseArguments<Options>(args)
                .MapResult(
                    opts => RunAddAndReturnExitCode(opts),
                    errs => 1);
            
        }

        public static int RunAddAndReturnExitCode(Options opts)
        {
            var relativeProjectPath = "project/views.json";
            
            var settings = CompilerRunner.GetBuildSettings(opts.Directory, relativeProjectPath, new SharpJsonSerializer(), true);

            var newViewSettings = new ViewsSettings();
            IViewsSettings optsSettings = opts;
            newViewSettings.Imports = optsSettings.Imports.Union(settings.Imports).ToArray();
            newViewSettings.References = optsSettings.References.Union(settings.References)
                .Select(x => new Reference { HintPath = x.HintPath, Name = x.Name })
                .ToArray();
            
            newViewSettings.LinkerPath = Fold(opts, settings, x => x.LinkerPath);
            newViewSettings.OutputFile = Fold(opts, settings, x => x.OutputFile);
            newViewSettings.RelativeIntermediateDirectory = Fold(opts, settings, x => x.RelativeIntermediateDirectory);
            newViewSettings.RelativeSourceDirectory = Fold(opts, settings, x => x.RelativeSourceDirectory);
            newViewSettings.SecondaryOutputFile = Fold(opts, settings, x => x.SecondaryOutputFile);
            newViewSettings.SkipSecondaryOutputFile = opts.SkipSecondaryOutputFile || settings.SkipSecondaryOutputFile;
            
            var logger = opts.Verbose
                ? (ILogger)new TextWriterLogger(Console.Out)
                : new NullLogger();
            
            if (opts.SingleRun)
            {
                using (var compiler = CompilerRunner.StartWatching(CodeTypes.FSharp, opts.Directory, settings, new ConsoleMessenger(), logger))
                {
                    return compiler.Compile() == CompileResult.Success ? 0 : -1;
                }
            }
            
            Console.WriteLine("Watching Directory '{0}'...", opts.Directory);
            Console.WriteLine("Press 'Control + Z' to exit");
            
            using (var compiler = CompilerRunner.StartWatching(CodeTypes.FSharp, opts.Directory, settings, new ConsoleMessenger(), logger))
            {
                compiler.Compile();
                var isControlC = false;
                while (!isControlC)
                {
                    var keyInfo = Console.ReadKey();
                    isControlC = keyInfo.Key == ConsoleKey.Z
                        && keyInfo.Modifiers == ConsoleModifiers.Control;
                }
            }
            
            return 0;
        }

        private static string Fold(IViewsSettings opts, IViewsSettings settings, Func<IViewsSettings, string> par)
        {
            var nonDefault = par(opts);
            if (!string.IsNullOrWhiteSpace(nonDefault))
            {
                return nonDefault;
            }
            
            return par(settings);
        }
    }
}

