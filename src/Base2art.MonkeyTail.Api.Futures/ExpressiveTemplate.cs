// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

using System;
namespace Base2art.MonkeyTail.Api
{
    public static class ExpressiveTemplate
    {
        [CLSCompliant(false)]
        public static TAppendable _if<TAppendable, TFormattable>(
            this ExpressiveTemplateBase<TAppendable, TFormattable> template,
            bool shouldRender,
            TAppendable appendable)
            where TAppendable : class, IAppendable<TAppendable>, new()
            where TFormattable : IFormat<TAppendable>
        {
            if (shouldRender)
            {
                return appendable;
            }
            
            return new TAppendable();
        }
        
        [CLSCompliant(false)]
        public static TAppendable _unless<TAppendable, TFormattable>(
            this ExpressiveTemplateBase<TAppendable, TFormattable> template,
            bool shouldRender,
            TAppendable appendable)
            where TAppendable : class, IAppendable<TAppendable>, new()
            where TFormattable : IFormat<TAppendable>
        {
            if (!shouldRender)
            {
                return appendable;
            }
            
            return new TAppendable();
        }
    }
}

