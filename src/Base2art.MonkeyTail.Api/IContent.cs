// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    public interface IContent
    {
        string Body { get; }

        string ContentType { get; }
    }
}

