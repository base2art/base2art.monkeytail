// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.ContentTypes
{
    using System.Text;

    public class JavascriptFormat : IFormat<Javascript>
    {
        public Javascript Raw(string value)
        {
            return new Javascript(new StringBuilder(value));
        }

        public Javascript Escape(string value)
        {
            return new Javascript(CleanForJson(value));
        }

        //http://stackoverflow.com/a/17691629/390632
        public static StringBuilder CleanForJson(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return new StringBuilder();
            }

            int i;
            int len = s.Length;
            StringBuilder sb = new StringBuilder(len + 4);

            for (i = 0; i < len; i += 1)
            {
                char c = s[i];
                switch (c)
                {
                    case '\\':
                    case '"':
                        sb.Append('\\');
                        sb.Append(c);
                        break;
                    case '/':
                        sb.Append('\\');
                        sb.Append(c);
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    default:
                        if (c < ' ')
                        {
                            string t = "000" + string.Format("X", c);
                            sb.Append("\\u" + t.Substring(t.Length - 4));
                        }
                        else
                        {
                            sb.Append(c);
                        }

                        break;
                }
            }
            return sb;
        }
    }
}

