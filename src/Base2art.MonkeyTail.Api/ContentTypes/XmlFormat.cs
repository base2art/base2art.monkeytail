// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.ContentTypes
{
    using System.Text;
    using System.Xml;

    public class XmlFormat : IFormat<Xml>
    {
        public Xml Raw(string value)
        {
            return new Xml(new StringBuilder(value));
        }

        public Xml Escape(string value)
        {
            return new Xml(new StringBuilder(XmlEscape(value)));
        }

        public static string XmlEscape(string unescaped)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = doc.CreateElement("root");
            node.InnerText = unescaped;
            return node.InnerXml;
        }
    }
}

