// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.ContentTypes
{
    using System.IO;
    using System.Text;

    public class HtmlFormat : IFormat<Html>
    {
        public Html Raw(string value)
        {
            return new Html(new StringBuilder(value));
        }

        public Html Escape(string value)
        {
            StringBuilder sb = new StringBuilder(value.Length);
            using (var stringWriter = new StringWriter(sb))
            {
                System.Net.WebUtility.HtmlEncode(value, stringWriter);
                stringWriter.Flush();
            }

            return new Html(sb);
        }
    }
}

