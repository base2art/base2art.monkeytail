// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.ContentTypes
{
    using System.Text;

    public class Javascript : 
        ContentBase, IAppendable<Javascript>, IContent
    {
        public Javascript()
            : this(new StringBuilder())
        {
        }
        
        public Javascript(StringBuilder buffer)
            : base(buffer)
        {
        }
        
        public static Javascript Empty()
        {
            return new Javascript(new StringBuilder());
        }

        public Javascript Append(Javascript paramT)
        {
            this.Buffer.Append(paramT.Buffer);
            return this;
        }

        public string ContentType
        {
            get
            {
                return "text/javascript";
            }
        }
    }
}

