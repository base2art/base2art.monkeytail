// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.ContentTypes
{
    using System.Text;

    public class TxtFormat : IFormat<Txt>
    {
        public Txt Raw(string value)
        {
            return new Txt(new StringBuilder(value));
        }

        public Txt Escape(string value)
        {
            return this.Raw(value);
        }
    }
}

