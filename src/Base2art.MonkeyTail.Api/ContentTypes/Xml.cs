// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.ContentTypes
{
    using System.Text;

    public class Xml : 
        ContentBase, IAppendable<Xml>, IContent
    {
        public Xml()
            :this(new StringBuilder())
        {
        }
        
        public Xml(StringBuilder buffer)
            : base(buffer)
        {
        }
        
        public static Xml Empty()
        {
            return new Xml(new StringBuilder());
        }

        public Xml Append(Xml paramT)
        {
            this.Buffer.Append(paramT.Buffer);
            return this;
        }

        public string ContentType
        {
            get
            {
                return "text/xml";
            }
        }
    }
}

