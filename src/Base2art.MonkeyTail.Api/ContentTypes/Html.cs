// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.ContentTypes
{
    using System.Text;

    public class Html : 
        ContentBase, IAppendable<Html>, IContent
    {
        public Html()
            : this(new StringBuilder())
        {
        }
        
        public Html(StringBuilder buffer)
            : base(buffer)
        {
        }
        
        public static Html Empty()
        {
            return new Html(new StringBuilder());
        }

        public Html Append(Html paramT)
        {
            this.Buffer.Append(paramT.Buffer);
            return this;
        }

        public string ContentType
        {
            get
            {
                return "text/html";
            }
        }
    }
}

