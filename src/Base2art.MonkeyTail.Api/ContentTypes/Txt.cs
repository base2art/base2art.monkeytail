// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.ContentTypes
{
    using System.Text;

    public class Txt : 
        ContentBase, IAppendable<Txt>, IContent
    {
        public Txt()
            :this(new StringBuilder())
        {
        }
        
        public Txt(StringBuilder buffer)
            :base(buffer)
        {
        }
        
        public static Txt Empty()
        {
            return new Txt(new StringBuilder());
        }

        public string ContentType
        {
            get
            {
                return "text/plain";
            }
        }

        public Txt Append(Txt paramT)
        {
            this.Buffer.Append(paramT.Buffer);
            return this;
        }
    }
}

