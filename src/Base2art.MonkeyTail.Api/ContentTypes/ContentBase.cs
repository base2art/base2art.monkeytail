// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.ContentTypes
{
    using System.Text;

    public class ContentBase
    {
        private readonly StringBuilder buffer;

        public ContentBase(StringBuilder value)
        {
            this.buffer = value;
        }

        public StringBuilder Buffer
        {
            get
            {
                return this.buffer;
            }
        }

        public string Body
        {
            get
            {
                return this.ToString();
            }
        }

        public override string ToString()
        {
            return this.Buffer.ToString();
        }
    }
}

