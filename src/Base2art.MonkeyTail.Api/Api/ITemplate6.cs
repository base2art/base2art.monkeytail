// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Api
{
    public interface ITemplate6<in T1, in T2, in T3, in T4, in T5, in T6, out TResult>
    {
        TResult Render(T1 item1, T2 item2, T3 item3, T4 item4, T5 item5, T6 item6);
    }
}

