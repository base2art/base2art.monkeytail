// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Api
{
    public interface ITemplate4<in T1, in T2, in T3, in T4, out TResult>
    {
        TResult Render(T1 item1, T2 item2, T3 item3, T4 item4);
    }
}

