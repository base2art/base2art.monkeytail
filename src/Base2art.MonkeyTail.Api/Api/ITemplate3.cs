// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Api
{
    public interface ITemplate3<in T1, in T2, in T3, out TResult>
    {
        TResult Render(T1 item1, T2 item2, T3 item3);
    }
}

