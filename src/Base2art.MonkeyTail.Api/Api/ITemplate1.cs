// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Api
{
    public interface ITemplate1<in T1, out TResult>
    {
        TResult Render(T1 item1);
    }
}

