﻿

#    public interface ITemplate1<in T1, out TResult>
#    {
#        TResult Render(T1 item1);
#    }

for i in range(1, 21):
  fh = open('ITemplate%s.cs' %(i), 'w+')
  a = []
  b = []
  for ii in range(1, i + 1):
    a.append("in T%d" % (ii))
    b.append("T%d item%d" % (ii, ii))

  fh.write("""// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Api
{
    public interface ITemplate%d<%s, out TResult>
    {
        TResult Render(%s);
    }
}
""" % (i, ", ".join(a), ", ".join(b)))
  fh.close()
