// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
	using System;
    using System.Collections.Generic;
    using System.Linq;
    
    public abstract class ExpressiveTemplateBase<TAppendable, TFormattable>
        where TAppendable : class, IAppendable<TAppendable>
        where TFormattable : IFormat<TAppendable>
    {
        private readonly TFormattable format;

        protected ExpressiveTemplateBase(TFormattable format)
        {
            this.format = format;
        }

        public TFormattable Format
        {
            get
            {
                return this.format;
            }
        }
        
        public TAppendable Raw(object text)
        {
            return this.Convert(text, x => this.Format.Raw(x));
        }
        
        public TAppendable Escape(object text)
        {
            return this.Convert(text, x => this.Format.Escape(x));
        }

        public TAppendable Agg(IEnumerable<TAppendable> enumerable)
        {
            return enumerable.Aggregate(this.Create(), (x, y) => x.Append(y));
        }

        protected virtual TAppendable Create()
        {
            return this.Format.Raw(string.Empty);
        }

		protected virtual TAppendable Convert(object text, Func<string, TAppendable> conversionFunction)
		{
            if (text == null)
            {
                return this.Create();
            }
            
            var text2 = text as string;
            if (text2 != null)
            {
                return conversionFunction(text2);
            }
            
            var html = text as TAppendable;
            if (html != null)
            {
                return html;
            }
            
            var listOfStrings = text as IEnumerable<string>;
            if (listOfStrings != null)
            {
                return this.Agg(listOfStrings.Select(conversionFunction));
            }
            
            var listOfHtml = text as IEnumerable<TAppendable>;
            if (listOfHtml != null)
            {
                return this.Agg(listOfHtml);
            }
            
            return conversionFunction(text.ToString());
		}
    }
}

