// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    using System.Linq;
    using Base2art.MonkeyTail.CodeDom;
    using Base2art.MonkeyTail.Config;
    using Base2art.MonkeyTail.Diagnostics;
    using Base2art.MonkeyTail.IO;
    using Base2art.MonkeyTail.LexerParser;

    public class Parser
    {
        private readonly TemplateEnumerator enumerator;

        private readonly string viewsOutputDir;

        private readonly IViewsSettings viewSettings;

        private readonly string workingDirectory;

        private readonly ICodeType codeType;
        
        public Parser(
            ICodeType codeType,
            TemplateEnumerator enumerator,
            IViewsSettings viewSettings,
            string workingDirectory, 
            string viewsOutputDir)
        {
            this.codeType = codeType;
            this.workingDirectory = workingDirectory;
            this.enumerator = enumerator;
            this.viewSettings = viewSettings;
            this.viewsOutputDir = viewsOutputDir;
        }
        
        public ISubPrcoessResult Parse(ILogger logger)
        {
            var filesToLink = new List<LinkData>();
            ITemplateParser templateParser = this.CreateParser(this.codeType);
            Dictionary<string, int> map = new Dictionary<string, int>();
            foreach (FileData entry in this.enumerator)
            {
                var relativePath = Path.ChangeExtension(entry.RelativePath, string.Empty).TrimEnd('.');
                var tempOutputPath = Path.Combine(this.viewsOutputDir, relativePath);
                
                var output = Path.ChangeExtension(tempOutputPath, string.Empty);
                output = output.TrimEnd('.');
                
                if (!map.ContainsKey(entry.FileNameNoExtension))
                {
                    map[entry.FileNameNoExtension] = -1;
                }
                
                map[entry.FileNameNoExtension] = map[entry.FileNameNoExtension] + 1;
                
                if (map[entry.FileNameNoExtension] > 0)
                {
                    output += "_" + map[entry.FileNameNoExtension];
                }
                
                output = Path.ChangeExtension(output, this.GetExtension(entry));
                Directory.CreateDirectory(Path.GetDirectoryName(output));

                using (var readAllText = File.OpenRead(entry.OriginalPath))
                {
                    using (var r = new StreamReader(readAllText, true))
                    {
                        string x = r.ReadToEnd();

                        using (var ms = new MemoryStream())
                        {
                            using (var streamWriter = new StreamWriter(ms))
                            {
                                streamWriter.Write(x);
                                streamWriter.Flush();

                                ms.Seek(0L, SeekOrigin.Begin);

                                var outText = templateParser.Parse(ms, entry);
                                File.WriteAllText(output, outText.ReturnValue.FileContent ?? string.Empty);
                                if (outText.ResultCode != 0)
                                {
                                    return outText;
                                }
                                
                                filesToLink.Add(new LinkData
                                    {
                                        Document = outText.ReturnValue.Document,
                                        FileContent = outText.ReturnValue.FileContent,
                                        Path = output,
                                        Id = this.ToId(Path.ChangeExtension(relativePath, string.Empty))
                                    });
                            }
                        }
                    }
                }
            }
            
            var files = this.ResolveOrder(filesToLink);

            return templateParser.Link(
                this.viewSettings,
                this.workingDirectory,
                files,
                logger);
        }

        private ITemplateParser CreateParser(ICodeType templateCodeType)
        {
            if (templateCodeType == CodeTypes.FSharp)
            {
                return new FSharpTemplateParser();
            }

            throw new ArgumentOutOfRangeException();
        }

        private string[] ResolveOrder(List<LinkData> filesToLink)
        {
            var collection = new DelegatedTopologicallySortedCollection<string, LinkData>(
                                 x => x.Id,
                                 x => x.Document.Requirements.Select(y => y.ToUpperInvariant()));
            
            filesToLink.ForEach(collection.Add);
            return collection.Resolve().Select(x => x.Path).ToArray();
        }

        private string ToId(string relativePath)
        {
            return relativePath
		        .TrimEnd('.')
		        .Replace('\\', '/')
		        .Replace('/', '.')
		        .ToUpperInvariant();
        }
		
        private string GetExtension(FileData entry)
        {
            if (entry != null && entry.CodeType != null)
            {
                return entry.CodeType.Extension;
            }
            
            throw new ArgumentOutOfRangeException();
        }
        
        private class LinkData
        {
            public string FileContent { get; set; }
            
            public IDocument Document { get; set; }
            
            public string Path { get; set; }
            
            public string Id { get; set; }
        }
    }
}

