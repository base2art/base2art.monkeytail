// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public static class CharExtension
    {
        public static bool NotIn(this char chr, char[] items)
        {
            return items.All(x => x != chr);
        }
        
        public static bool In(this char chr, char[] items)
        {
            return items.Any(x => x == chr);
        }
        
        public static bool IsNewLine(this char chr)
        {
            return chr == '\r' || chr == '\n';
        }

        public static bool IsColon(this char chr)
        {
            return chr == ':';
        }

        public static bool IsEOF(this char chr)
        {
            return chr == char.MaxValue;
        }

        public static void AppendFormatLine(this StringBuilder sb, string format, params object[] values)
        {
            sb.AppendFormat(format, values);
            sb.AppendLine();
        }
        
        public static IEnumerable<int> Range(this int iters)
        {
            for (int i = 0; i < iters; i++)
            {
                yield return i;
            }
        }
        
        // http://stackoverflow.com/questions/2070356/find-common-prefix-of-strings
        public static string FindCommonPrefix(this string[] ss)
        {
            if (ss == null)
            {
                return string.Empty;
            }
            
            if (ss.Length == 0)
            {
                return string.Empty;
            }

            if (ss.Length == 1)
            {
                return ss[0];
            }

            int prefixLength = 0;

            foreach (char c in ss[0])
            {
                foreach (string s in ss)
                {
                    if (s.Length <= prefixLength || s[prefixLength] != c)
                    {
                        return ss[0].Substring(0, prefixLength);
                    }
                }
                prefixLength++;
            }

            return ss[0]; // all strings identical
        }
    }
}

