// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class DelegatedTopologicallySortedCollection<TKey, TItem>
        : TopologicallySortedCollection<TKey, TItem>
        where TKey : IComparable<TKey>
        where TItem : class
    {
        private readonly Func<TItem, IEnumerable<TKey>> refsFunc;

        private readonly Func<TItem, TKey> keyFunc;
        
        public DelegatedTopologicallySortedCollection(Func<TItem, TKey> keyFunc, Func<TItem, IEnumerable<TKey>> refsFunc)
        {
            if (refsFunc == null)
            {
                throw new ArgumentNullException("refsFunc");
            }
			
            if (keyFunc == null)
            {
                throw new ArgumentNullException("keyFunc");
            }
            
            this.keyFunc = keyFunc;
            this.refsFunc = refsFunc;    
        }
        
        protected override TKey GetKey(TItem item)
        {
            return this.keyFunc(item);
        }
        
        protected override IEnumerable<TKey> GetDependencies(TItem item)
        {
            return this.refsFunc(item);
        }
    }
}

