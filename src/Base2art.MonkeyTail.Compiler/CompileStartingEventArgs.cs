// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    using System;
    using System.Collections.Generic;

    public class CompileStartingEventArgs : EventArgs
    {
        private readonly IList<CompilableFile> files = new List<CompilableFile>();

        public IEnumerable<ICompilableFile> Files
        {
            get { return new List<ICompilableFile>(this.files); }
        }
		
        public void Add(string path, CompilePriority priority)
        {
            this.files.Add(new CompilableFile(path, priority));
        }
		
        private class CompilableFile : ICompilableFile
        {
            private readonly string path;

            private readonly CompilePriority priority;

            public CompilableFile(string path, CompilePriority priority)
            {
                this.priority = priority;
                this.path = path;
            }
		    
            string ICompilableFile.Path
            {
                get { return this.path; }
            }
            
            CompilePriority ICompilableFile.Priority
            {
                get { return this.priority; }
            }
        }
    }
}

