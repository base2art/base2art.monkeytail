// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.CodeDom
{
    using System.Collections.Generic;

    public interface IContainerNode : INode
    {
        IEnumerable<KeyValuePair<string, string>> Attributes { get; }

        IReadOnlyList<INode> ChildNodes { get; }
    }
}

