// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.CodeDom
{
    public interface IProcessingInstruction : INode
    {
        string[] Value { get; }

        bool Raw { get; }
        
        IContainerNode Inner { get; }
    }
}

