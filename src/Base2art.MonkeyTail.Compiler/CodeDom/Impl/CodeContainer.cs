// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.CodeDom.Impl
{
    using System.Collections.Generic;

    public class CodeContainer : IEditableContainerNode
    {
        private readonly List<INode> children = new List<INode>();

        private readonly List<KeyValuePair<string, string>> attrs = new List<KeyValuePair<string, string>>();

		private readonly IEditableContainerNode parent;
		
        public CodeContainer(IEditableContainerNode parent)
        {
            this.parent = parent;   
        }
        
        public void Add(INode childNode)
        {
            this.children.Add(childNode);
        }

        public void SetAttribute(string key, string value)
        {
            this.attrs.Add(new KeyValuePair<string, string>(key, value));
        }
        
        public IEditableContainerNode Parent
        {
            get
            {
                return this.parent;
            }
        }
        
        public IEnumerable<KeyValuePair<string, string>> Attributes
        {
            get
            {
                return this.attrs;
            }
        }
        
        public IReadOnlyList<INode> ChildNodes
        {
            get
            {
                return this.children;
            }
        }
    }
}

