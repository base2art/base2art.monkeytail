// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.CodeDom.Impl
{
    using System.Collections.Generic;

    using System.Text;
    using System.Text.RegularExpressions;
    using Base2art.MonkeyTail.CodeDom;

    public class CodeDocument : IDocument
    {
        private readonly string ns;

        private readonly string className;

        private readonly List<string> references = new List<string>();

        private readonly List<string> requirements = new List<string>();

        private readonly List<INode> children = new List<INode>();

        private readonly List<KeyValuePair<string, string>> attrs = new List<KeyValuePair<string, string>>();

        private readonly IFileType outputType;

        public CodeDocument(string ns, string className, IFileType outputType)
        {
            this.ns = ns;
            this.className = Clean(className);
            this.outputType = outputType;
        }

        public IReadOnlyList<INode> ChildNodes
        {
            get  { return this.children; }
        }

        public string Namespace
        {
            get { return this.ns; }
        }

        public IEditableContainerNode Parent
        {
            get { return null; }
        }
        
        public string Name
        {
            get { return this.className; }
        }

        public IFileType OutputType
        {
            get{ return this.outputType; }
        }

        public IEnumerable<KeyValuePair<string, string>> Attributes
        {
            get { return this.attrs; }
        }

        public string[] References
        {
            get { return this.references.ToArray(); }
        }
        
        public string[] Requirements
        {
            get { return this.requirements.ToArray(); }
        }
        public ITextNode CreateTextNode(string text)
        {
            return new CodeTextNode(text);
        }

        public void AddReferenceNotation(string reference)
        {
            this.references.Add(reference);
        }
        
        public void AddRequirementNotation(string requirement)
        {
            this.requirements.Add(requirement);
        }
        
        public IProcessingInstruction CreateProcessingInstruction(string text, bool raw)
        {
            return new CodeProcessingInstruction(text, raw, null);
        }

        public IEditableContainerNode CreateContainer(IEditableContainerNode parentNode)
        {
            return new CodeContainer(parentNode);
        }
        
        public IProcessingInstruction CreateProcessingInstruction(string[] text, bool raw)
        {
            return new CodeProcessingInstruction(text, raw, null);
        }
        
        public IProcessingInstruction CreateProcessingInstruction(string text, bool raw, IContainerNode inner)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                if (text.Trim().StartsWith("$", System.StringComparison.Ordinal) && !text.Trim().StartsWith("$_", System.StringComparison.Ordinal))
                {
                    var regex = new Regex(Regex.Escape("$"));
                    text = regex.Replace(text, "this._", 1);
                }
            }
            
            return new CodeProcessingInstruction(text, raw, inner);
        }
        
        public IProcessingInstruction CreateProcessingInstruction(string[] text, bool raw, IContainerNode inner)
        {
            return new CodeProcessingInstruction(text, raw, inner);
        }

        public IComment CreateComment(string text)
        {
            return new CodeComment(text);
        }

        public void Add(INode childNode)
        {
            this.children.Add(childNode);
        }

        public void SetAttribute(string key, string value)
        {
            this.attrs.Add(new KeyValuePair<string, string>(key, value));
        }

        private static string Clean(string className)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var chr in className)
            {
                if (char.IsLetterOrDigit(chr) || chr == '_')
                {
                    sb.Append(chr);
                } else if (chr == '-')
                {
                    sb.Append('_');
                }
            }
            
            return sb.ToString();
        }
    }
}

