// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.CodeDom.Impl
{
    using System.Collections.Generic;
 
    public class CodeProcessingInstruction : IProcessingInstruction
    {
        private readonly string[] value;

        private readonly bool raw;

        private IContainerNode inner;

        public CodeProcessingInstruction(string value, bool raw, IContainerNode inner)
        {
            this.inner = inner;
            this.value = new string[]{ value };
            this.raw = raw;
        }

        public CodeProcessingInstruction(string[] value, bool raw, IContainerNode inner)
        {
            this.inner = inner;
            this.value = value;
            this.raw = raw;
        }

        public string[] Value
        {
            get
            {
                return new List<string>(this.value ?? new string[0]).ToArray();
            }
        }

        public bool Raw
        {
            get
            {
                return this.raw;
            }
        }

        public IContainerNode Inner
        {
            get
            {
                return this.inner;
            }
        }
		
        public override string ToString()
        {
            return string.Format("[CodeProcessingInstruction Value={0}]", string.Join("\\n", value));
        }
    }
}

