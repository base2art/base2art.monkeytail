// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.CodeDom.Impl
{
    using Base2art.MonkeyTail.CodeDom;

    public class CodeComment : IComment
    {
        private readonly string text;

        public CodeComment(string text)
        {
            this.text = text;
        }

        public string Text
        {
            get
            {
                return this.text;
            }
        }
    }
}

