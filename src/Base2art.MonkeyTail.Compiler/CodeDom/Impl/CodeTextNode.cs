// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.CodeDom.Impl
{
    public class CodeTextNode : ITextNode
    {
        private readonly string text;

        public CodeTextNode(string text)
        {
            this.text = text;
        }

        public string Text
        {
            get
            {
                return this.text;
            }
        }
        
        public override string ToString()
        {
			return string.Format("[CodeTextNode Text={0}]", text);
        }
    }
}

