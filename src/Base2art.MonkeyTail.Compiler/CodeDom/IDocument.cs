// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.CodeDom
{
    public interface IDocument : IEditableContainerNode
    {
        string Namespace { get; }

        string Name { get; }

        IFileType OutputType { get; }
        
        string[] References { get; }
        
        string[] Requirements { get; }
        
        ITextNode CreateTextNode(string text);

        IProcessingInstruction CreateProcessingInstruction(string text, bool raw);

        IProcessingInstruction CreateProcessingInstruction(string[] text, bool raw);

        IProcessingInstruction CreateProcessingInstruction(string text, bool raw, IContainerNode inner);

        IProcessingInstruction CreateProcessingInstruction(string[] text, bool raw, IContainerNode inner);

        IComment CreateComment(string text);

        void AddReferenceNotation(string reference);

        void AddRequirementNotation(string requirement);
    }
}

