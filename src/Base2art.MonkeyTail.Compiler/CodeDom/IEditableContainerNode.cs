// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.CodeDom
{
    public interface IEditableContainerNode : IContainerNode
    {
        IEditableContainerNode Parent { get; }

        void Add(INode node);

        void SetAttribute(string key, string value);
    }
}

