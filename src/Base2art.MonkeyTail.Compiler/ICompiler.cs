// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    using System;
    
    public interface ICompiler : IDisposable
    {
        event EventHandler<EventArgs> CompileStarting;
        event EventHandler<EventArgs> CompileCompleting;
        event EventHandler<CompileCompletedEventArgs> CompileCompleted;
        
        CompileResult Compile();
    }
}

