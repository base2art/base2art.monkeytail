// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    public class FileData
    {
        public IFileType FileType { get; set; }

        public ICodeType CodeType { get; set; }

        public string RelativePath { get; set; }

        public string OriginalPath { get; set; }

        public string FileNameNoExtension { get; set; }

        public string Namespace { get; set; }
        
        public override string ToString()
        {
			return string.Format("[FileData FileType={0}, CodeType={1}, RelativePath={2}, OriginalPath={3}, FileNameNoExtension={4}, Namespace={5}]", FileType, CodeType, RelativePath, OriginalPath, FileNameNoExtension, Namespace);
        }
    }
}

