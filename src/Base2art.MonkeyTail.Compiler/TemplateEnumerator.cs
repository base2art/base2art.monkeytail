// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class TemplateEnumerator : IEnumerable<FileData>
    {
        private readonly string path;

        private readonly IFileType[] fileTypes;

        private readonly ICodeType templateCodeType;
        
        [Obsolete("Use the overload that takes FileTypes")]
        public TemplateEnumerator(string path) : this(CodeTypes.FSharp, path, FileTypes.KnownValues)
        {
        }
        
        public TemplateEnumerator(ICodeType templateCodeType, string path, IFileType[] fileTypes)
        {
            this.templateCodeType = templateCodeType;
            this.fileTypes = fileTypes;
            this.path = path;
        }
        
        public IEnumerator<FileData> GetEnumerator()
        {
            var dirInfo = new DirectoryInfo(this.path);
            var items = new List<FileData>();
            this.Recurse(items, dirInfo, new[] { "Views" });
            return items.GetEnumerator();
        }

        private void Recurse(List<FileData> items, DirectoryInfo dirInfo, string[] strings)
        {
            foreach (var info in dirInfo.EnumerateDirectories())
            {
                var list = new List<string>(strings);
                list.Add(info.Name);
                this.Recurse(items, info, list.ToArray());
            }
            
            foreach (IFileType fileType in this.fileTypes)
            {
                AddFile(this.templateCodeType, items, dirInfo, strings, fileType);
            }
        }

        private static void AddFile(
            ICodeType templateCodeType,
            List<FileData> items,
            DirectoryInfo dirInfo,
            string[] strings,
            IFileType fileType)
        {
            string searchPattern = "*." + templateCodeType.Extension + "." + fileType.Extension;
            foreach (var file in dirInfo.EnumerateFiles(searchPattern, SearchOption.TopDirectoryOnly))
            {
                List<string> list = new List<string>(strings);
                list.Insert(1, fileType.Name);
                var codeType = GetCodeType(file);
                if (codeType == null)
                {
                    continue;
                }

                items.Add(
                    new FileData
                    {
                        FileType = fileType,
                        RelativePath = string.Join("\\", list) + "\\" + file.Name,
                        OriginalPath = file.FullName,
                        CodeType = codeType,
                        FileNameNoExtension = NoExtension(file.Name),
                        Namespace =  string.Join(".", list.Skip(2))
                    });
            }
        }

        private static string NoExtension(string name)
        {
            if (!name.Contains("."))
            {
                return name;
            }

            return NoExtension(Path.GetFileNameWithoutExtension(name));
        }

        private static ICodeType GetCodeType(FileInfo file)
        {
            var extension = Path.GetExtension(Path.GetFileNameWithoutExtension(file.FullName));
            switch (extension)
            {
                case ".fs":
                    return CodeTypes.FSharp;
//                case ".cs":
//                    return CodeType.CSharp;
            }

            return null;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}

