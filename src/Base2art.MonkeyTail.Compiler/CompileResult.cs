namespace Base2art.MonkeyTail
{
    public enum CompileResult
    {
        Success,
        SuccessWithWarnings,
        Failed
    }
}

