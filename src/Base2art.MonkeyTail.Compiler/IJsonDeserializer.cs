namespace Base2art.MonkeyTail
{
    using System.IO;

    public interface IJsonDeserializer
    {
        T Deserialize<T>(string sr);

        string Serialize<T>(T settings);
    }
}

