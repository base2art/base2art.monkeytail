// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    using System;

    public class CompileCompletedEventArgs : EventArgs
    {
        private readonly CompileResult compileResult;

        public CompileCompletedEventArgs(CompileResult compileResult)
        {
            this.compileResult = compileResult;
        }
	    
        public CompileResult CompileResult
        {
            get { return this.compileResult; }
        }
    }
}

