// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Diagnostics
{
	public class NullLogger : ILogger
	{
		public void Log(string message)
		{
		}
	}
}

