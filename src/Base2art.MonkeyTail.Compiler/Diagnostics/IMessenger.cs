// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Diagnostics
{
    public interface IMessenger
    {
        void Warning(string message);

        void Info(string message);
        
        void Error(string message);
    }
}

