// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    using System;

    using Base2art.MonkeyTail.Diagnostics;

    public class ConsoleMessenger : IMessenger
    {
        public void Warning(string message)
        {
            WriteWithColor(message, ConsoleColor.Yellow);
        }

        public void Info(string message)
        {
            WriteWithColor(message, ConsoleColor.White);
        }

        public void Error(string message)
        {
            WriteWithColor(message, ConsoleColor.Red);
        }

        private static void WriteWithColor(string message, ConsoleColor c)
        {
            var consoleColor = Console.ForegroundColor;
            Console.ForegroundColor = c;
            Console.WriteLine(message);
            Console.ForegroundColor = consoleColor;
        }
    }
}

