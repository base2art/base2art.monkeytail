// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

using System.IO;
namespace Base2art.MonkeyTail.Diagnostics
{
    public class TextWriterLogger : ILogger
    {
		private readonly TextWriter logger;

        public TextWriterLogger(TextWriter logger)
        {
            this.logger = logger;
        }
        
        public void Log(string message)
        {
            if (this.logger != null)
            {
                this.logger.WriteLine(message);
            }
        }
    }
}

