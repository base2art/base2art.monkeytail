// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.IO
{
    public class Error : IError
    {
        private readonly string message;

        public Error(string message)
        {
            this.message = message;
        }

        public string Message
        {
            get
            {
                return this.message;
            }
        }
        
        public override string ToString()
        {
			return string.Format("[Error Message={0}]", message);
        }
    }
}

