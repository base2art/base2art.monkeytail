// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.IO
{
	using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;

    public static class ProcessRunner
    {
        public static ProcessEndInfo Execute(string path, string arguments, String workingDir)
        {
            using (Process p = new Process())
            {
                ProcessStartInfo psI = new ProcessStartInfo(path);
                if (!string.IsNullOrWhiteSpace(workingDir)) 
                {
                    psI.WorkingDirectory =  workingDir;
                }
                psI.UseShellExecute = false;
                psI.RedirectStandardInput = true;
                psI.RedirectStandardOutput = true;
                psI.RedirectStandardError = true;
                psI.CreateNoWindow = true;
                psI.Arguments = arguments;
                p.StartInfo = psI;
                try
                {
                    p.Start();
                } 
                catch (Win32Exception e) 
                {
                    return new ProcessEndInfo(
                        string.Empty,
						string.Format("Check that the file '{0}' exists.{1}{2}", path, Environment.NewLine, e), 
                        int.MinValue);
                }

                StreamReader err = p.StandardError;
                string stderr = err.ReadToEnd();
                StreamReader sr = p.StandardOutput;
                string stdout = sr.ReadToEnd();
                //                Thread.Sleep(500);

                //                p.StandardInput.WriteLine();

                p.WaitForExit();

                return new ProcessEndInfo(stdout, stderr, p.ExitCode);
            }
        }
    }
}

////                            if (tbComm.Text != "")
////                            {
////                                sw.WriteLine(tbComm.Text);
////                            }
////                            else
////                            {
////                                //execute default command
////                                sw.WriteLine("dir \\");
////                            }

