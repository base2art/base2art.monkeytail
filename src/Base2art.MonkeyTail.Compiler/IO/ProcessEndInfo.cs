// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.IO
{
    public class ProcessEndInfo
    {
        private readonly string standardOutput;

        private readonly string errorOutput;

        private readonly int exitCode;

        public ProcessEndInfo(string standardOutput, string errorOutput, int exitCode)
        {
            this.standardOutput = standardOutput;
            this.errorOutput = errorOutput;
            this.exitCode = exitCode;
        }

        public int ExitCode
        {
            get
            {
                return this.exitCode;
            }
        }

        public string ErrorOutput
        {
            get
            {
                return this.errorOutput;
            }
        }

        public string StandardOutput
        {
            get
            {
                return this.standardOutput;
            }
        }
    }
}

