// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.IO
{
    public class SubProcessResult<T> : ISubPrcoessResult<T>
    {
        public SubProcessResult()
        {
            this.ResultCode = 0;
            this.Errors = new IError[0];
            this.Warnings = new IError[0];
            this.Messages = new string[0];
        }

        public int ResultCode { get; set; }

        public IError[] Errors { get; set; }

        public IError[] Warnings { get; set; }

        public string[] Messages { get; set; }

        public T ReturnValue { get; set; }
    }
}

