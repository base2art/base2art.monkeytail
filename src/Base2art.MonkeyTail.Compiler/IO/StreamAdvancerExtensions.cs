// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.IO
{
    using System;
    using System.Text;

    public static class StreamAdvancerExtensions
    {
        public static string ReadUntilConsecutive(this StreamAdvancer stream, char chr, params char[] chrs)
        {
            if (chrs == null || chrs.Length == 0)
            {
                return stream.ReadUntil(x => x == chr);
            }

            StringBuilder sb = new StringBuilder();
            return stream.ReadUntilConsecutiveInternal(sb, chr, chrs);
        }

        public static string ReadUntil(this StreamAdvancer stream, Func<char, bool> test)
        {
            return stream.ReadUntil(test, false);
        }

        public static string ReadUntil(this StreamAdvancer stream, Func<char, bool> test, bool trim)
        {
            StringBuilder name = new StringBuilder();
            while (!test(stream.Current) && !stream.Current.IsEOF())
            {
                name.Append(stream.Current);
                stream.Advance();
            }

            var readUntil = name.ToString();
            return trim ? readUntil.Trim() : readUntil;
        }

        public static string ReadUntilConsecutiveInternal(
            this StreamAdvancer stream,
            StringBuilder sb,
            char chr,
            params char[] chrs)
        {

            sb.Append(stream.ReadUntil(x => x == chr, false));
            var next = stream.Current;
            stream.Advance();
            if (stream.Current == chrs[0])
            {
                stream.Advance();
                return sb.ToString();
            }

            sb.Append(next);

            return stream.ReadUntilConsecutiveInternal(sb, chr, chrs);
        }
    }
}

