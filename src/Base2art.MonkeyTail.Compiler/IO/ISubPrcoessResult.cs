// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.IO
{
    public interface ISubPrcoessResult
    {
        int ResultCode { get; }

        IError[] Errors { get; }

        IError[] Warnings { get; }

        string[] Messages { get; }
    }

    public interface ISubPrcoessResult<out T> : ISubPrcoessResult
    {
        T ReturnValue { get; }
    }
}

