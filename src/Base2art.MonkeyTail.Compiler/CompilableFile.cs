// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    public interface ICompilableFile
    {
        string Path { get; }
        CompilePriority Priority { get; }
    }
}

