﻿// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
	public static class CodeTypes
	{
		public readonly static ICodeType FSharp = new InnerCodeType("FSharp", "fs");

		public static ICodeType[] KnownValues
		{
			get
			{
				return new[] {
					FSharp
				};
			}
		}

		private class InnerCodeType : ICodeType
		{
			private readonly string name;

			private readonly string extension;

			public InnerCodeType(string name, string extension)
			{
				this.name = name;
				this.extension = extension;
			}

			public string Extension
			{
				get
				{
					return this.extension;
				}
			}

			public string Name
			{
				get
				{
					return name;
				}
			}

			public override string ToString()
			{
				return this.name;
			}
		}
	}
}

