// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using System.Collections.Generic;
    using Base2art.MonkeyTail.CodeDom;
    
    public abstract class MethodInvokation
    {
        private readonly MethodInvokation parentNode;
        private readonly int positionInParent;
        private readonly IContainerNode node;
        
        private readonly List<MethodInvokation> childItems = new List<MethodInvokation>();
		
        protected MethodInvokation(IContainerNode node, MethodInvokation parentNode, int positionInParent)
        {
            this.node = node;
            this.parentNode = parentNode;
            this.positionInParent = positionInParent;
        }

        public IContainerNode Node
        {
            get
            {
                return node;
            }
        }
        
        public MethodInvokation ParentNode
        {
            get
            {
                return this.parentNode;
            }
        }

        public List<MethodInvokation> Items
        {
            get{ return this.childItems; }
        }
        
        public int PositionInParent
        {
            get
            {
                return positionInParent;
            }
        }
		
        public abstract string BodyText
        {
            get;
        }

        public abstract string[] AnonymousChildParams { get; }
        
        public abstract bool CanHaveLogic { get; }
		
        public abstract bool RequiresEscaping { get; }

        public abstract string MethodName();
        
        private void GetStackId(List<int> ids)
        {
            if (this.ParentNode == null)
            {
                return;
            }
		    
            this.ParentNode.GetStackId(ids);
            ids.Add(this.positionInParent);
        }
        
        protected string GetStackId()
        {
            List<int> i = new List<int>();
            this.GetStackId(i);
            return string.Join("_", i);
        }
    }
}

