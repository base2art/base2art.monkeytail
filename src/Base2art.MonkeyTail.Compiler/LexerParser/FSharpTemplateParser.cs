// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using System.IO;

    using Base2art.MonkeyTail.CodeDom;
    using Base2art.MonkeyTail.Config;
    using Base2art.MonkeyTail.Diagnostics;
    using Base2art.MonkeyTail.IO;

    public class FSharpTemplateParser : ITemplateParser
    {
        public ISubPrcoessResult<IDocument> Lex(Stream stream, FileData data)
        {
            return new FSharpCodeTemplateLexer(new StreamAdvancer(stream), data).Lex();
        }

        public ISubPrcoessResult<string> Assemble(IDocument doc)
        {
            FSharpCodeTemplateAssembler assembler = new FSharpCodeTemplateAssembler(doc);
            return assembler.Assemble();
        }

        public ISubPrcoessResult<string> Link(IViewsSettings settings, string workingDirectory, string[] pathsToLink, ILogger logger)
        {
            FSharpCodeTemplateLinker linker = new FSharpCodeTemplateLinker(settings, workingDirectory, pathsToLink, logger);
            return linker.Link();
        }

        public ISubPrcoessResult<IParseResult> Parse(Stream stream, FileData data)
        {
            var document = this.Lex(stream, data);
            ISubPrcoessResult rez = document;
            var pr = new ParseResult();
            if (document.ResultCode == 0)
            {
                var innerRezult = this.Assemble(document.ReturnValue);
                rez = innerRezult;
                pr.Document = document.ReturnValue;
                pr.FileContent = innerRezult.ReturnValue;
            }
                
            return new SubProcessResult<IParseResult> {
                ResultCode = rez.ResultCode,
                Errors = rez.Errors,
                Warnings = rez.Warnings,
                Messages = rez.Messages,
                ReturnValue = pr
            };
        }
        
        private class ParseResult : IParseResult
        {
            public string FileContent{ get; set; }
            
            public IDocument Document{ get; set; }
        }
    }
}

