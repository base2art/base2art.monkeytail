// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using System.Linq;
    using Base2art.MonkeyTail.CodeDom;
    
    public class ProcessingInstructionMethodInvokation : MethodInvokation
    {
        private readonly bool requiresEscaping;

        private readonly IProcessingInstruction node;
		
        public ProcessingInstructionMethodInvokation(IProcessingInstruction node, MethodInvokation parentNode, int positionInParent, bool requiresEscaping)
            : base(node.Inner, parentNode, positionInParent)
        {
            this.node = node;
            this.requiresEscaping = requiresEscaping;
        }

        public override bool RequiresEscaping
        {
            get
            {
                return this.requiresEscaping;
            }
        }
        public override string BodyText
        {
            get
            {
                var text = RawBodyText();
                text = text.Replace("$_", this.VarName(0))
                    .Replace("$_0", this.VarName(0))
                    .Replace("$_1", this.VarName(1))
                    .Replace("$_2", this.VarName(2))
                    .Replace("$_3", this.VarName(3))
                    .Replace("$_4", this.VarName(4))
                    .Replace("$_5", this.VarName(5))
                    .Replace("$_6", this.VarName(6))
                    .Replace("$_7", this.VarName(7))
                    .Replace("$_8", this.VarName(8))
                    .Replace("$_9", this.VarName(9));
                
                var childrenParams = this.ChildrenParamsMax();
                
                var format = childrenParams.Length == 0 ? "this.{1}({2})" : "fun ({0}) -> this.{1}({2})";
                
                var functionCall = string.Format(
                                       format,
                                       string.Join(", ", childrenParams),
                                       this.MethodName() + "Agg",
                                       this.ParameterListInvocation(true));
                return text.Replace("$func", functionCall);
            }
        }

        public override string[] AnonymousChildParams
        {
            get
            {
                var text = string.Join(System.Environment.NewLine, this.node.Value);
                return text.Contains("$_") ? new string[]{ this.VarName(0) } : new string[0];
            }
        }
		
        public override bool CanHaveLogic
        {
            get
            {
                return true;
            }
        }
        
        public override string MethodName()
        {
            return "ProcInst" + this.GetStackId();
        }
        
        private string RawBodyText()
        {
            if (this.node.Value.Length == 0)
            {
                return "\"\"";
            }
            
            if (this.node.Value.Length == 1)
            {
                return node.Value[0];
            }
            
            var lines = node.Value.Where(x => x != null).Where(x => !string.IsNullOrWhiteSpace(x.Trim())).ToArray();
            var charsCountToRemove = lines.FindCommonPrefix().TakeWhile(char.IsWhiteSpace).Count();
            var codeLines = lines.Select(x => System.Environment.NewLine + new string(' ', 8) + x.Substring(charsCountToRemove));
            return string.Join(string.Empty, codeLines);
        }

        private string VarName(int i)
        {
            return "v_" + this.GetStackId() + "_p_" + i;
        }
    }
}

