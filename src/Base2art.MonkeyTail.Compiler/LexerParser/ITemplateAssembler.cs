// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using Base2art.MonkeyTail.IO;

    public interface ITemplateAssembler
    {
        ISubPrcoessResult<string> Assemble();
    }
}

