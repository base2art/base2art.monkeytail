// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Base2art.MonkeyTail.CodeDom;
    using Base2art.MonkeyTail.IO;

    public class FSharpCodeTemplateAssembler : ITemplateAssembler
    {
        private readonly IDocument doc;

        public FSharpCodeTemplateAssembler(IDocument doc)
        {
            this.doc = doc;
        }

        public ISubPrcoessResult<string> Assemble()
        {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrWhiteSpace(this.doc.Namespace))
            {
                sb.AppendFormatLine("namespace Views.{0}", this.doc.OutputType);
            } else
            {
                sb.AppendFormatLine("namespace Views.{0}.{1}", this.doc.OutputType, this.doc.Namespace);
            }

            sb.AppendLine("");
            sb.AppendLine("open Base2art.MonkeyTail");
            sb.AppendLine("open Base2art.MonkeyTail.Api");
            sb.AppendLine("open Base2art.MonkeyTail.ContentTypes");
            
            foreach (var reference in this.doc.References)
            {
                sb.Append("open ");
                sb.AppendLine(reference);
            }
            
            sb.AppendLine("");
            sb.AppendFormatLine("type {0}() =", this.doc.Name);
            sb.AppendLine("");

            var paramsCount = this.doc.Attributes.Count();

            sb.AppendFormatLine(
                "  inherit ExpressiveTemplateBase<{0}, IFormat<{0}>>(new {0}Format())",
                this.doc.OutputType.Name);
            sb.AppendLine("");

            if (paramsCount == 0)
            {
                sb.AppendFormatLine(
                    "  interface ITemplate0<{0}> with",
                    this.doc.OutputType.Name);
            } else
            {
                sb.AppendFormatLine(
                    "  interface ITemplate{0}<{1}, {2}> with",
                    paramsCount,
                    string.Join(", ", this.doc.Attributes.Select(c => c.Value)),
                    this.doc.OutputType.Name);
            }

            var root = new NullMethodInvokation(this.doc);
            
            sb.AppendFormatLine(
                "    member this.Render({0}) = this.Render({1})",
                root.ParameterList(),
                root.ParameterListInvocation());

            sb.AppendLine("  end");
            sb.AppendLine("");
            sb.AppendLine("  with");

            List<MethodInvokation> items = new List<MethodInvokation>();
            this.CreateMethodInvokationData(items, new NullMethodInvokation(this.doc));

            sb.AppendFormatLine(
                "    static member Apply({1}) = (new {0}()).Render({2})",
                this.doc.Name,
                root.ParameterList(),
                root.ParameterListInvocation());

            sb.AppendFormatLine("    member this.Render({0}) = ", root.ParameterList());
            sb.AddInEscapeAndRawMethods(this.doc.OutputType);
            sb.AppendFormatLine("         {0}", this.RenderChildAppends(items));
            
            this.RenderChildItems(items, sb);

            return new SubProcessResult<string> { ReturnValue = sb.ToString() };
        }

        private void CreateMethodInvokationData(
            List<MethodInvokation> aggregate, 
            MethodInvokation parentInvocation)
        {
            if (parentInvocation.Node == null || parentInvocation.Node.ChildNodes == null)
            {
                return;
            }
            
            for (int i = 0; i < parentInvocation.Node.ChildNodes.Count; i++)
            {
                
                var childNode = parentInvocation.Node.ChildNodes[i];
				
                var textNode = childNode as ITextNode;
                if (textNode != null)
                {
                    aggregate.Add(new TextMethodInvokation(parentInvocation.Node, parentInvocation, i, this.RenderTextNode(textNode)));
                }
				
                var commentNode = childNode as IComment;
                if (commentNode != null)
                {
// SWALLOW COMMENT
                }
				
                var instruction = childNode as IProcessingInstruction;
                if (instruction != null)
                {
                    var invokation = new ProcessingInstructionMethodInvokation(instruction, parentInvocation, i, !instruction.Raw);
                    aggregate.Add(invokation);
                    this.CreateMethodInvokationData(invokation.Items, invokation);
                }
            }
        }

        private string RenderTextNode(ITextNode node)
        {
            return string.Concat("\"", node.Text.Replace("\"", "\\\""), "\"");
        }

        private string RenderChildAppends(List<MethodInvokation> items)
        {
            if (items.Count == 0)
            {
                return "Raw(\"\")";
            }

            StringBuilder sb = new StringBuilder();

            var firstItem = items[0];
            this.AppendMethod(sb, firstItem);

            for (int index = 1; index < items.Count; index++)
            {
                var item = items[index];
                sb.Append(".Append(");
                this.AppendMethod(sb, item);
                sb.Append(")");
            }

            sb.AppendLine();

            return sb.ToString();
        }

        private void RenderChildItems(List<MethodInvokation> items, StringBuilder sb)
        {
            foreach (var item in items)
            {
                if (item.CanHaveLogic)
                {
                    var parms = item.ParameterList();
                    sb.AppendFormatLine(
                        "    member this.{0}({1}) = {2}",
                        item.MethodName(),
                        parms,
                        item.BodyText);
                } else
                {
                    sb.AppendFormatLine("    member this.{0} = {1}", item.MethodName(), item.BodyText);
                }
            }
            
            foreach (var item in items)
            {  
                var parms = item.ParameterList(true);
                if (item.Items.Count > 0)
                {
                    sb.AppendFormatLine(
                        "    member this.{0}Agg({1}) = ",
                        item.MethodName(),
                        parms
                    );
                    
                    var maxWords = item.ChildrenParamsMax();
                    foreach (var element in item.Items)
                    {
                        var current = element.AnonymousChildParams;
                        for (int i = 0; i < current.Length; i++)
                        {
                            var currWord = current[i];
                            var currMaxWord = maxWords[i];
                            if (!string.Equals(currMaxWord, currWord))
                            {
                                sb.AppendFormatLine("         let {0} = {1}", currWord, currMaxWord);
                            }
                        }
                    }
                    
                    sb.AddInEscapeAndRawMethods(this.doc.OutputType);
                    sb.AppendFormatLine("         {0}", this.RenderChildAppends(item.Items));
                }
            }
            
            foreach (var item in items)
            {
                if (item.Items.Count > 0)
                {
                    this.RenderChildItems(item.Items, sb);
                }
            }
        }
		
        private void AppendMethod(StringBuilder sb, MethodInvokation methodData)
        {
            string name = methodData.CanHaveLogic ? 
                (methodData.RequiresEscaping ? "this.Escape(" : "this.Raw(") 
                : "this.Raw(";

            sb.Append(name);
            sb.Append("this.");
            sb.Append(methodData.MethodName());
            if (methodData.CanHaveLogic)
            {
                sb.Append("(");
                sb.Append(methodData.ParameterListInvocation());
                sb.Append(")");
            }

            sb.Append(")");
        }
    }
}

