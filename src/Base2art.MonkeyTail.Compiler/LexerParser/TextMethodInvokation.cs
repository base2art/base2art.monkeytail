// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using Base2art.MonkeyTail.CodeDom;

    public class TextMethodInvokation : MethodInvokation
    {
        private readonly string bodyText;

        public TextMethodInvokation(IContainerNode node, MethodInvokation parentNode, int positionInParent, string bodyText)
            : base(node, parentNode, positionInParent)
        {
            this.bodyText = bodyText;
        }

        public override string[] AnonymousChildParams
		{
			get
			{
			    return new string[0];
			}
		}

        public override string BodyText
        {
            get
            {
                return bodyText;
            }
        }
		
        public override bool CanHaveLogic
        {
            get
            {
                return false;
            }
        }
		
        public override bool RequiresEscaping
        {
            get
            {
                return false;
            }
        }
        
        public override string MethodName()
        {
            return "Text" + this.GetStackId();
        }
    }
}

