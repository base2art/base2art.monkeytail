// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    using Base2art.MonkeyTail.CodeDom;
    using Base2art.MonkeyTail.IO;

    public class FSharpCodeTemplateAssemblerBackup : ITemplateAssembler
    {
        private readonly IDocument doc;

        public FSharpCodeTemplateAssemblerBackup(IDocument doc)
        {
            this.doc = doc;
        }

        public ISubPrcoessResult<string> Assemble()
        {
            StringBuilder sb = new StringBuilder();
            if (string.IsNullOrWhiteSpace(this.doc.Namespace))
            {
                sb.AppendLine("namespace Views.Html");
            } else
            {
                sb.AppendFormatLine("namespace Views.Html.{0}", this.doc.Namespace);
            }

            sb.AppendLine("");
            sb.AppendLine("open Base2art.MonkeyTail");
            sb.AppendLine("open Base2art.MonkeyTail.Api");
            sb.AppendLine("open Base2art.MonkeyTail.ContentTypes");
            sb.AppendLine("");
            sb.AppendFormatLine("type {0}() =", this.doc.Name);
            sb.AppendLine("");

            var paramsCount = this.doc.Attributes.Count();

            sb.AppendFormatLine(
                "  inherit ExpressiveTemplateBase<{0}, IFormat<{0}>>(new {0}Format())",
                this.doc.OutputType.ToString("G"));
            sb.AppendLine("");

            if (paramsCount == 0)
            {
                sb.AppendFormatLine(
                    "  interface ITemplate0<{0}> with",
                    this.doc.OutputType.ToString("G"));
            } else
            {
                sb.AppendFormatLine(
                    "  interface ITemplate{0}<{1}, {2}> with",
                    paramsCount,
                    string.Join(", ", this.doc.Attributes.Select(c => c.Value)),
                    this.doc.OutputType.ToString("G"));
            }

            sb.AppendFormatLine(
                "    member this.Render({0}) = this.Render({1})",
                this.ParameterList(this.doc),
                this.ParameterListInvocation(this.doc));

            sb.AppendLine("  end");
            sb.AppendLine("");
            sb.AppendLine("  with");

            List<MethodData> items = new List<MethodData>();
            this.RenderCollection(items, this.doc, new int[0]);

            sb.AppendFormatLine(
                "    static member Apply({1}) = (new {0}()).Render({2})",
                this.doc.Name,
                this.ParameterList(this.doc),
                this.ParameterListInvocation(this.doc));

            sb.AppendFormatLine("    member this.Render({0}) = ", this.ParameterList(this.doc));
            this.AddInEscapeAndRawMethods(sb);
            sb.AppendFormatLine("         {0}", this.MethodBody(items, this.doc));
            
            this.RenderChildItems(items, sb);

            return new SubProcessResult<string> { ReturnValue = sb.ToString() };
        }
        
        public void RenderChildItems(List<MethodData> items, StringBuilder sb)
        {
            foreach (var item in items)
            {
                if (item.HasParams)
                {
                    List<string> parmVals = new List<string>();
                    for (int i = 0; i < item.ParameterCount; i++)
                    {
                        parmVals.Add("v_" + string.Join("_", item.Stack) + "_p_" + i);
                    }
                    
                    var parms = this.ParameterList(this.doc);
                    if (string.IsNullOrWhiteSpace(parms))
                    {
                        parms = string.Join(", ", parmVals);
                    } else if (parmVals.Count > 0)
                    {
                        parms += ", " + string.Join(", ", parmVals);
                    }
                    
                    sb.AppendFormatLine(
                        "    member this.{0}({1}) = {2}",
                        item.MethodName,
                        parms,
                        item.BodyText);
                } else
                {
                    sb.AppendFormatLine("    member this.{0} = {1}", item.MethodName, item.BodyText);
                }
            }
            
            foreach (var item in items)
            {
                
                if (item.Items.Count > 0)
                {
                    this.RenderChildItems(item.Items, sb);
                }
            }
        }

        private void AddInEscapeAndRawMethods(StringBuilder sb)
        {
            sb.AppendFormatLine("         let Escape(text:System.Object) =");
            sb.AppendFormatLine("                                match text with");
            sb.AppendFormatLine("                                            | :? System.String as x -> this.Format.Escape(x)");
            sb.AppendFormatLine("                                            | :? Html as html -> html");
            sb.AppendFormatLine("                                            | :? System.Collections.Generic.IEnumerable<System.String> as x -> List.ofSeq(x) |> List.map(fun y -> this.Format.Escape(y)) |> List.fold(fun runningSum nextVal -> runningSum.Append(nextVal)) (new Html(new System.Text.StringBuilder()))");
            sb.AppendFormatLine("                                            | :? System.Collections.Generic.IEnumerable<Html> as x -> List.ofSeq(x) |> List.fold(fun runningSum nextVal -> runningSum.Append(nextVal)) (new Html(new System.Text.StringBuilder()))");
            sb.AppendFormatLine("                                            | line -> this.Format.Escape(line.ToString())");
            sb.AppendFormatLine("         let Raw(text:System.Object) =");
            sb.AppendFormatLine("                                match text with");
            sb.AppendFormatLine("                                            | :? System.String as x -> this.Format.Raw(x)");
            sb.AppendFormatLine("                                            | :? Html as html -> html");
            sb.AppendFormatLine("                                            | :? System.Collections.Generic.IEnumerable<System.String> as x -> List.ofSeq(x) |> List.map(fun y -> this.Format.Raw(y)) |> List.fold(fun runningSum nextVal -> runningSum.Append(nextVal)) (new Html(new System.Text.StringBuilder()))");
            sb.AppendFormatLine("                                            | :? System.Collections.Generic.IEnumerable<Html> as x -> List.ofSeq(x) |> List.fold(fun runningSum nextVal -> runningSum.Append(nextVal)) (new Html(new System.Text.StringBuilder()))");
            sb.AppendFormatLine("                                            | line -> this.Format.Raw(line.ToString())");
            sb.AppendLine();
        }

        public Tuple<string, int> RenderProcessingInstruction(IProcessingInstruction node, int[] stack)
        {
            if (node.Value.Length == 0)
            {
                return Tuple.Create("\"\"", 0);
            }
            
            if (node.Value.Length == 1)
            {
                return ProcessSingleInst(node.Value[0], node, stack);
            }
            
            var lines = node.Value.Where(x => x != null)
                        .Where(x => !string.IsNullOrWhiteSpace(x.Trim()))
                        .ToArray();
            
            var charsCountToRemove = lines.FindCommonPrefix()
                .TakeWhile(char.IsWhiteSpace)
                .Count();
            
            var codeLines = lines.Select(x => Environment.NewLine + new string(' ', 8) + x.Substring(charsCountToRemove));
            return ProcessSingleInst(string.Join(string.Empty, codeLines), node, stack);
        }

        private void RenderCollection(List<MethodData> items, IContainerNode container, int[] trace)
        {
            int i = 0;
            foreach (var node in container.ChildNodes)
            {
                var stack = this.CopyAnd(trace, i);
                
                string suffix = this.MethodNameSuffix(stack);
                
                var textNode = node as ITextNode;
                if (textNode != null)
                {
                    var text = this.RenderTextNode(textNode);
                    items.Add(new MethodData("Text" + suffix, false, text, false, new int[]{ }, 0, false));
                }

                var instruction = node as IProcessingInstruction;
                if (instruction != null)
                {
                    var processingInstruction = this.RenderProcessingInstruction(instruction, stack);
                    
                    var md = new MethodData("ProcInst" + suffix, true, processingInstruction.Item1, !instruction.Raw, stack, processingInstruction.Item2, false);
                    
                    var hasChildren = instruction.Inner != null && instruction.Inner.ChildNodes != null;
                    
                    items.Add(md);
                    
                    if (hasChildren)
                    {
                        this.RenderCollection(md.Items, instruction.Inner, stack.ToArray());
                        StringBuilder sb = new StringBuilder();
                        sb.AppendLine();
                        this.AddInEscapeAndRawMethods(sb);
                        sb.AppendFormatLine("         {0}", this.MethodBody(md.Items, instruction.Inner));
                        var mda = new MethodData("ProcInstAgg" + suffix, true, sb.ToString(), !instruction.Raw, stack, processingInstruction.Item2, true);
                        items.Add(mda);
                    }
                }

                var commentNode = node as IComment;
                if (commentNode != null)
                {
                    //                    var comment = this.RenderComment(commentNode);
                    //                    items.Add(new MethodData("Comment" + items.Count, false, comment));
                }
                
                i++;
            }
        }

        private string RenderTextNode(ITextNode node)
        {
            return string.Concat("\"", node.Text.Replace("\"", "\\\""), "\"");
        }

        private string MethodBody(List<MethodData> items, IContainerNode container)
        {
            List<MethodData> filteredItems = new List<MethodData>(items.Where(x => !x.IsAggregator));
            if (filteredItems.Count == 0)
            {
                return "this.Format.Raw(\"\")";
            }

            StringBuilder sb = new StringBuilder();

            var firstItem = filteredItems[0];
            this.AppendMethod(container, sb, firstItem);

            for (int index = 1; index < filteredItems.Count; index++)
            {
                var item = filteredItems[index];
                sb.Append(".Append(");
                this.AppendMethod(container, sb, item);
                sb.Append(")");
            }

            sb.AppendLine();

            return sb.ToString();
        }

        private string ParameterList(IContainerNode containerNode)
        {
            return string.Join(", ", containerNode.Attributes.Select(c => c.Key + ":" + c.Value));
        }

        private string ParameterListInvocation(IContainerNode containerNode)
        {
            return string.Join(", ", containerNode.Attributes.Select(c => c.Key));
        }

        private void AppendMethod(IContainerNode container, StringBuilder sb, MethodData methodData)
        {
            string name = methodData.HasParams ? (methodData.RequiresEscape ? "Escape(" : "Raw(") : "this.Format.Raw(";

            sb.Append(name);
            sb.Append("this.");
            sb.Append(methodData.MethodName);
            if (methodData.HasParams)
            {
                sb.Append("(");
                sb.Append(this.ParameterListInvocation(container));
                var a = methodData.Items.Aggregate<MethodData, int>(0, (x, y) => x + y.ParameterCount);
                if (container.Attributes.Any())
                {
                    if (methodData.Items.Count > 0 && a > 0)
                    {
                        sb.Append(",");
                    }
                }
                
                foreach (var child in (methodData.Items))
                {
                    for (int i = 0; i < child.ParameterCount; i++)
                    {
                        sb.Append(this.VarName(child.Stack, i));
                    }
                }
                
                sb.Append(")");
            }

            sb.Append(")");
        }

        private int GetVariableCount(string str)
        {
            for (int i = 9; i >= 0; i--)
            {
                if (str.Contains("$_" + i))
                {
                    return i + 1;
                }
            }
		    
            return str.Contains("$_") ? 1 : 0;
        }

        private Tuple<string, int> ProcessSingleInst(string procInstValue, IProcessingInstruction node, int[] stack)
        {
            var max = this.GetVariableCount(procInstValue);
            procInstValue = procInstValue
                .Replace("$_9", this.VarName(stack, 9))
                .Replace("$_8", this.VarName(stack, 8))
                .Replace("$_7", this.VarName(stack, 7))
                .Replace("$_6", this.VarName(stack, 6))
                .Replace("$_5", this.VarName(stack, 5))
                .Replace("$_4", this.VarName(stack, 4))
                .Replace("$_3", this.VarName(stack, 3))
                .Replace("$_2", this.VarName(stack, 2))
                .Replace("$_1", this.VarName(stack, 1))
                .Replace("$_0", this.VarName(stack, 0))
                .Replace("$_", this.VarName(stack, 0));
			
            var tempFuncParams = string.Join(", ", max.Range().Select(x => this.VarName(stack, x)));
            if (max != 0)
            {
                procInstValue = procInstValue.Replace("$func", "fun (" + tempFuncParams + ") -> this.ProcInstAgg" + this.MethodNameSuffix(stack) + "(items, " + tempFuncParams + ")");
            } else
            {
                procInstValue = procInstValue.Replace("$func", "fun () -> this.ProcInstAgg" + this.MethodNameSuffix(stack) + "(items)");
            }
            return Tuple.Create(procInstValue, max);
        }

        private string MethodNameSuffix(int[] stack)
        {
            return string.Join("_", stack);
        }

        private string VarName(int[] stack, int i)
        {
            return "v_" + string.Join("_", stack) + "_p_" + i;
        }

        private int[] CopyAnd(int[] trace, int i)
        {
            var stack = new List<int>(trace);
            stack.Add(i);
            return stack.ToArray();
        }
    }
}

