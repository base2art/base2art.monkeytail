// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using System.IO;

    using Base2art.MonkeyTail.CodeDom;
    using Base2art.MonkeyTail.Config;
	using Base2art.MonkeyTail.Diagnostics;
    using Base2art.MonkeyTail.IO;

    public interface ITemplateParser
    {
        ISubPrcoessResult<IDocument> Lex(Stream stream, FileData data);

        ISubPrcoessResult<string> Assemble(IDocument doc);

        ISubPrcoessResult<string> Link(IViewsSettings viewSettings, string workingDirectory, string[] pathsToLink, ILogger logger);

        ISubPrcoessResult<IParseResult> Parse(Stream stream, FileData data);
    }
}

