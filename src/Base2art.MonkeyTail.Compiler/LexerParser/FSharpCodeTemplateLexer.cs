// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using System.CodeDom.Compiler;
    using System.Collections.Generic;

    using Base2art.MonkeyTail.CodeDom;
    using Base2art.MonkeyTail.CodeDom.Impl;
    using Base2art.MonkeyTail.IO;
    using Base2art.MonkeyTail.LexerParser.Langs;

    public class FSharpCodeTemplateLexer : ITemplateLexer
    {
        private List<string> buf = new List<string>();

        private readonly HeaderLexer headerReader;

        private readonly BodyLexer bodyReader;

        FileData fileData;
        public FSharpCodeTemplateLexer(StreamAdvancer stream, FileData fileData)
        {
            this.fileData = fileData;
            this.headerReader = new HeaderLexer(new HeaderTokenizer(stream));
            this.bodyReader = new BodyLexer(new BodyTokenizer(stream));
        }

        public ISubPrcoessResult<IDocument> Lex()
        {
            var doc = new CodeDocument(this.fileData.Namespace, this.fileData.FileNameNoExtension, this.fileData.FileType);
            var returnValue = new SubProcessResult<IDocument> {
                ReturnValue = doc,
                ResultCode = 0
            };
            
            try
            {
                foreach (var param in this.headerReader.Parameters())
                {
                    doc.SetAttribute(param.Name, param.TypeName);
                }
            
                foreach (var reference in this.headerReader.References())
                {
                    doc.AddReferenceNotation(reference);
                }
            
                foreach (var require in this.headerReader.Requires())
                {
                    doc.AddRequirementNotation(require);
                }
            
                this.bodyReader.Lex(doc);

            } catch (CompilationException ce)
            {
                returnValue.ResultCode = 1;
                returnValue.Errors = new IError[]{ new Error(ce.Message) };
            }
            
            return returnValue;
        }
    }
}

