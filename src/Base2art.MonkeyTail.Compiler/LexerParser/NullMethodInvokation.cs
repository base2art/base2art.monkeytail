﻿// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using Base2art.MonkeyTail.CodeDom;
    
    public class NullMethodInvokation : MethodInvokation
    {
        public NullMethodInvokation(IContainerNode doc)
            : base(doc, null, 0)
        {
        }

        public override string[] AnonymousChildParams
		{
			get
			{
			    return new string[0];
			}
		}
		
        public override string BodyText
        {
            get
            {
                return string.Empty;
            }
        }
		
        public override bool CanHaveLogic
        {
            get
            {
                return false;
            }
        }
		
        public override bool RequiresEscaping
        {
            get
            {
                return false;
            }
        }
        
        public override string MethodName()
        {
            return string.Empty;
        }
    }
}

