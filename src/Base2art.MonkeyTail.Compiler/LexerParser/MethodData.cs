// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

using System.Collections.Generic;
namespace Base2art.MonkeyTail.LexerParser
{
    public class MethodData
    {
        private readonly List<MethodData> items = new List<MethodData>();
        
        public MethodData(string methodName, bool hasParams, string bodyText, bool requiresEscape, int[] stack, int pCount, bool isAggregator)
        {
            this.MethodName = methodName;
            this.HasParams = hasParams;
            this.BodyText = bodyText;
            this.RequiresEscape = requiresEscape;
            this.Stack = stack;
            this.ParameterCount = pCount;
            this.IsAggregator = isAggregator;
        }

        public string MethodName { get; set; }
        
        public bool HasParams { get; set; }
        
        public string BodyText { get; set; }

        public bool RequiresEscape { get; set; }
        
        public int[] Stack { get; set; }
        
        public int ParameterCount { get; set; }
        
        public bool IsAggregator { get; set; }
        
        public List<MethodData> Items
        {
            get { return this.items; }
        }
    }
}

