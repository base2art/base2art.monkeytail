//// <copyright company="Base2art">
//// Copyright (c) 2014 All Rights Reserved
//// </copyright>
//// <author>Scott Youngblut</author>
//
//namespace Base2art.MonkeyTail.LexerParser.Langs
//{
//    using System;
//    using System.Collections.Generic;
//    using System.Linq;
//    using Base2art.MonkeyTail.CodeDom.Impl;
//    using Base2art.MonkeyTail.IO;
//    
//    public class BodyExpressionLexer
//    {
//        private readonly IEnumerator<IToken<BodyTokenType>> tokenizer1;
//
//        private readonly BodyLexer lexer;
//
//        private readonly CodeDocument parentDoc;
//        
//        public BodyExpressionLexer(CodeDocument parentDoc, BodyLexer lexer, IEnumerator<IToken<BodyTokenType>> tokenizer1)
//        {
//            this.parentDoc = parentDoc;
//            this.lexer = lexer;
//            this.tokenizer1 = tokenizer1;
//        }
//        
//        public void Lex(CodeDocument doc)
//        {
//            this.ExpressionEscape(doc, this.tokenizer1, new LinkedList<IToken<BodyTokenType>>());
//        }
//        
//        private void ExpressionMethodBody(
//            CodeDocument doc, 
//            IEnumerator<IToken<BodyTokenType>> tokenizer,
//            LinkedList<IToken<BodyTokenType>> preAgg,
//            LinkedList<IToken<BodyTokenType>> agg)
//        {
//            var current = tokenizer.Current;
//            var tokenType = current.Type;
//            
//            if (tokenType == BodyTokenType.CloseBrace)
//            {
//                var processingInst = string.Concat(preAgg.Select(x => x.Value).ToArray());
//                
//                var trimmedProcIsnt = processingInst.Trim();
//                
//                if (trimmedProcIsnt.Contains("\r") || trimmedProcIsnt.Contains("\n"))
//                {
//                    string[] lines = processingInst.Split(new char[] {
//                        '\r',
//                        '\n'
//                    }, StringSplitOptions.RemoveEmptyEntries);
//                    
//                    lines = lines.Where(x => x != null)
//                        .Where(x => !string.IsNullOrWhiteSpace(x.Trim()))
//                        .ToArray();
//                    
//                    doc.Add(doc.CreateProcessingInstruction(lines, false, doc == this.parentDoc ? null : doc));
//                } else
//                {
//                    doc.Add(doc.CreateProcessingInstruction(trimmedProcIsnt, false, doc == this.parentDoc ? null : doc));
//                }
//                
//                if (tokenizer.MoveNext())
//                {
//                    this.lexer.Reset(this.parentDoc, tokenizer);
//                }
//                
//                return;
//            }
//            
//            if (!tokenizer.MoveNext())
//            {
//                throw new CompilationException("Expecting End Expression Escaped '}'");
//            }
//            
//            agg.AddLast(current);
//            this.ExpressionMethodBody(doc, tokenizer, preAgg, agg);
//        }
//    }
//}

