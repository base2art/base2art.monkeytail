// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    public enum HeaderTokenType
    {
        WhiteSpace,

        Word,

        Symbol,

        Keyword,

        Number,

        Pound,

        Dot,

        Dash,

        LessThan,

        GreaterThan,

        MonkeyTail,

        Colon,

        Comma,

        NewLine,

        EOF,
    }
}

