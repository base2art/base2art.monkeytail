// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Base2art.MonkeyTail.IO;
    
    public class BodyTokenizer : TokenizerBase<BodyTokenType>
    {
        protected override Dictionary<char, BodyTokenType> CreateSingleCharTokensLookup()
        {
            var items = base.CreateSingleCharTokensLookup();
            // COMMENT
            items.Add('*', BodyTokenType.Asterisk);
            
            // LITERAL OR COMMAND
            items.Add('@', BodyTokenType.MonkeyTail);
            
            // EXPR ESCAPED
            items.Add('{', BodyTokenType.OpenBrace);
            items.Add('}', BodyTokenType.CloseBrace);
            
            // EXPR RAW
            items.Add('<', BodyTokenType.LessThan);
            items.Add('>', BodyTokenType.GreaterThan);
            
            items.Add('\t', BodyTokenType.WhiteSpace);
            items.Add(' ', BodyTokenType.WhiteSpace);
            items.Add('\n', BodyTokenType.LF);
            items.Add('\r', BodyTokenType.CR);
            
            // Function
            items.Add('-', BodyTokenType.Dash);
            items.Add(':', BodyTokenType.Colon);
            
            return items;
        }

        public BodyTokenizer(StreamAdvancer stream)
            : base(stream)
        {
        }

        protected override BodyTokenType EndingToken
        {
            get
            {
                return BodyTokenType.EOF;
            }
        }

        protected override bool HandleMultiCharacterToken(LinkedList<Token<BodyTokenType>> tokens, StringBuilder sb, char c)
        {
            // NO MULTI CHARACTER TOKENS??
//            base.HandleMultiCharacterToken(tokens, sb, c);

            sb.Append(c);
            return this.Reset(tokens, sb, BodyTokenType.Ignore, true);
        }
    }
}

