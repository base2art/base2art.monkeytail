// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    public class Parameter : IParameter
    {
        public string Name { get; set; }

        public string TypeName { get; set; }
    }
}

