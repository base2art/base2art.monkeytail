// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Base2art.MonkeyTail.IO;

    public abstract class TokenizerBase<T> : ITokenizer<T>
        where T : struct
    {
        private readonly StreamAdvancer stream;
        
        private Dictionary<char, T> singleCharTokens;

        protected TokenizerBase(StreamAdvancer stream)
        {
            this.stream = stream;
        }

        protected abstract T EndingToken { get; }
        
        protected IReadOnlyDictionary<char, T> SingleCharTokens
        {
            get
            {
                if (this.singleCharTokens == null)
                {
                    this.singleCharTokens = this.CreateSingleCharTokensLookup();
                }
                
                return this.singleCharTokens;
            }
        }
        
        protected virtual Dictionary<char, T> CreateSingleCharTokensLookup()
        {
            return new Dictionary<char, T> {
                { char.MaxValue, this.EndingToken },
                { char.MinValue, this.EndingToken }
            };
        }
        
        public IEnumerable<IToken<T>> Parse()
        {
            var tokens = new LinkedList<Token<T>>();
            
            while (this.Token(tokens))
            {
            }
            var items = new List<IToken<T>>();
            
            foreach (var token in tokens)
            {
                items.Add(token);
            }
            
            return items;
        }

        protected bool Reset(LinkedList<Token<T>> tokens, StringBuilder current, T type)
        {
             return this.Reset(tokens, current, type, false);
        }

        protected bool Reset(LinkedList<Token<T>> tokens, StringBuilder current, T type, bool advance)
        {
            if (advance)
            {
                this.stream.Advance();
            }
            
            tokens.AddLast(new Token<T> {
                Type = type,
                Value = current.ToString()
            });
            
            if (type.Equals(this.EndingToken))
            {
                return false;
            }
            
            return true;
//            this.Token(tokens);
        }

        protected char Advance()
        {
            this.stream.Advance();
            return this.stream.Current;
        }

        protected bool Token(LinkedList<Token<T>> tokens)
        {
            //            this.stream.Advance();
            return this.HandleToken(tokens, new StringBuilder(), this.stream.Current);
        }

        protected bool HandleToken(LinkedList<Token<T>> tokens, StringBuilder sb, char c)
        {
            foreach (var singleCharToken in this.SingleCharTokens)
            {
                if (singleCharToken.Key == c)
                {
                    this.ProcessSingleCharToken(tokens, sb, c, singleCharToken.Value);
                    return !singleCharToken.Value.Equals(this.EndingToken);
                }
            }
            
            return this.HandleMultiCharacterToken(tokens, sb, c);
        }
        
        protected virtual bool HandleMultiCharacterToken(LinkedList<Token<T>> tokens, StringBuilder sb, char c)
        {
            return true;
        }
        
        protected virtual void ProcessSingleCharToken(
            LinkedList<Token<T>> tokens,
            StringBuilder sb,
            char c,
            T type)
        {
            sb.Append(c);
            this.Reset(tokens, sb, type, true);
        }
    }
}

//            }
//
//		private void ReadToken(StringBuilder sb)
//		{
//		    var c = stream.Current;
//		    if (c == '#')
//		    {
//		        sb.Append("#");
//		        return;
//		    }
//		}
//
//        public string NextRawToken
//        {
//            get
//            {
//                StringBuilder sb = new StringBuilder();
//                this.ReadToken(sb);
//                return ;
//            }
//        }

