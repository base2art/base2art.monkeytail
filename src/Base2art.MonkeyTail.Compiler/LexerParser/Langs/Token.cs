// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    public class Token<TTokenType> : IToken<TTokenType>
    {
        public TTokenType Type { get; set; }
        
        public string Value { get; set; }
        
        public override string ToString()
        {
            return string.Format("[Token Type={0}, Value={1}]", this.Type, this.Value);
        }
    }
}

