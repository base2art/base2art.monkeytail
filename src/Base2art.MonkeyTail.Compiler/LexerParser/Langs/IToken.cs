// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    public interface IToken<TTokenType>
    {
        TTokenType Type { get; }
        
        string Value { get; }
    }
}

