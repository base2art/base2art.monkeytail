// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Base2art.MonkeyTail.IO;
    
    public class HeaderTokenizer : TokenizerBase<HeaderTokenType>
    {
        public HeaderTokenizer(StreamAdvancer stream)
            : base(stream)
        {
        }
        
        protected override Dictionary<char, HeaderTokenType> CreateSingleCharTokensLookup()
        {
            var dict = base.CreateSingleCharTokensLookup();
            dict.Add('<', HeaderTokenType.LessThan);
            dict.Add('>', HeaderTokenType.GreaterThan);
            dict.Add('#', HeaderTokenType.Pound);
            dict.Add(':', HeaderTokenType.Colon);
            dict.Add(',', HeaderTokenType.Comma);
            dict.Add('.', HeaderTokenType.Dot);
            dict.Add('-', HeaderTokenType.Dash);
            return dict;
        }

        protected override HeaderTokenType EndingToken
        {
            get
            {
                return HeaderTokenType.EOF;
            }
        }
        
        protected override bool HandleMultiCharacterToken(
            LinkedList<Token<HeaderTokenType>> tokens,
            StringBuilder sb,
            char c)
        {
            base.HandleMultiCharacterToken(tokens, sb, c);
            
            if (c == '@')
            {
                return this.MonkeyTail(tokens, sb, c);
            }
            
            if (c.IsNewLine())
            {
                return this.NewLine(tokens, sb, c);
            }
            
            if (char.IsWhiteSpace(c) && !c.IsNewLine())
            {
                return this.Whitespace(tokens, sb, c);
            }
            
            if (char.IsLetter(c))
            {
                return this.Word(tokens, sb, c);
            }
            
            throw new CompilationException(c.ToString());
        }

        private bool Whitespace(LinkedList<Token<HeaderTokenType>> tokens, StringBuilder sb, char c)
        {
            sb.Append(c);
            var next = this.Advance();
            if (char.IsWhiteSpace(next) && !next.IsNewLine())
            {
                return this.Whitespace(tokens, sb, next);
            } else
            {
                return this.Reset(tokens, sb, HeaderTokenType.WhiteSpace);
            }
        }
        
        private bool Word(LinkedList<Token<HeaderTokenType>> tokens, StringBuilder sb, char c)
        {
            sb.Append(c);
            var next = this.Advance();
            if (char.IsLetterOrDigit(next))
            {
                return this.Word(tokens, sb, next);
            } else
            {
                return this.Reset(tokens, sb, HeaderTokenType.Word);
            }
        }
        
        private bool NewLine(LinkedList<Token<HeaderTokenType>> tokens, StringBuilder sb, char c)
        {
            var next = this.Advance();
            if (c == '\r' && next == '\n')
            {
                sb.Append(c);
                sb.Append(next);
                return this.Reset(tokens, sb, HeaderTokenType.NewLine, true);
            } else
            {
                sb.Append(c);
                return this.Reset(tokens, sb, HeaderTokenType.NewLine, false);
            }
        }
        
        private bool EndOfFile(LinkedList<Token<HeaderTokenType>> tokens, StringBuilder sb, char c)
        {
            sb.Append(c);
            return this.Reset(tokens, sb, HeaderTokenType.EOF, true);
        }
        
        private bool MonkeyTail(LinkedList<Token<HeaderTokenType>> tokens, StringBuilder sb, char c)
        {
            var next = this.Advance();
            if (next == '-')
            {
                sb.Append(c);
                //                sb.Append(next);
                return this.EndOfFile(tokens, sb, next);
            } else
            {
                sb.Append(c);
                return this.Reset(tokens, sb, HeaderTokenType.MonkeyTail, false);
            }
        }
    }
}

