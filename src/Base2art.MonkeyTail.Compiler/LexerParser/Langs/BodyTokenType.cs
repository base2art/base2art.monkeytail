// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    public enum BodyTokenType
    {
        Ignore,
        
        MonkeyTail,
        
        EOF,
        
        Asterisk,

        WhiteSpace,

        CR,

        LF,
        
        LessThan,
        
        GreaterThan,
        
        OpenBrace,
        
        CloseBrace,
        
        Dash,
        
        Colon,
        
        MethodBreak,
        //
        //
        //        Word,
        //
        //        Symbol,
        //
        //        Keyword,
        //
        //        Number,
        //
        //        Pound,
        //
        //        Dot,
        //
        //
        //        Comma,
        //
        //        NewLine,
        //
    }
}

