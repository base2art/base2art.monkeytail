// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    using System;
    using System.Diagnostics;
    using System.Linq;
    using System.Collections.Generic;
    using System.Text;
    using Base2art.MonkeyTail.CodeDom;
    using Base2art.MonkeyTail.CodeDom.Impl;
    
    public class BodyLexer
    {
        private readonly BodyTokenizer bodyTokenizer;
        
        private bool hasLexed;

        public BodyLexer(BodyTokenizer bodyTokenizer)
        {
            this.bodyTokenizer = bodyTokenizer;
        }
        
        public void Lex(CodeDocument doc)
        {
            this.EnsureLexed(doc);
        }
        
        private void EnsureLexed(CodeDocument doc)
        {
            if (!this.hasLexed)
            {
                this.LexInternal(doc);
                this.hasLexed = true;
            }
        }

        private void LexInternal(CodeDocument doc)
        {
            var results = this.bodyTokenizer.Parse().GetEnumerator();
            
            if (results.MoveNext())
            {
                this.SwallowSpace(results);
                this.Reset(doc, doc, results);
            }
        }
        
        private void SwallowSpace(IEnumerator<IToken<BodyTokenType>> tokenizer)
        {
            if (tokenizer.Current != null)
            {
                if (IsWhiteSpace(tokenizer.Current.Type))
                {
                    tokenizer.MoveNext();
                    this.SwallowSpace(tokenizer);
                }
            }
        }
        
        private void Statement(CodeDocument doc, IEditableContainerNode currentNode, IEnumerator<IToken<BodyTokenType>> tokenizer)
        {
            var currentToken = tokenizer.Current;
            if (currentToken.Type == BodyTokenType.MonkeyTail)
            {
                if (tokenizer.MoveNext())
                {
                    this.MokeyTail(doc, currentNode, tokenizer, currentToken);
                }
                else
                {
                    currentNode.Add(doc.CreateTextNode("@"));
                }
                
                return;
            }
            
//            if (currentToken.Type == BodyTokenType.CloseBrace && currentNode.Parent != null)
//            {
//                return;
//            }
            
            if (currentToken.Type == BodyTokenType.EOF)
            {
                return;
            }
            
            this.Literal(doc, currentNode, tokenizer, new StringBuilder());
        }
        
        private void Reset(CodeDocument doc, IEditableContainerNode currentNode, IEnumerator<IToken<BodyTokenType>> tokenizer)
        {
            this.Statement(doc, currentNode, tokenizer);
        }

        private void Literal(CodeDocument doc, IEditableContainerNode currentNode, IEnumerator<IToken<BodyTokenType>> tokenizer, StringBuilder stringBuilder)
        {
            if (tokenizer.Current.Type == BodyTokenType.MonkeyTail)
            {
                currentNode.Add(doc.CreateTextNode(stringBuilder.ToString()));
                this.Reset(doc, currentNode, tokenizer);
                return;
            }
            
            if (tokenizer.Current.Type == BodyTokenType.CloseBrace && currentNode.Parent != null)
            {
                currentNode.Add(doc.CreateTextNode(stringBuilder.ToString()));
                return;
            }
            
            if (tokenizer.Current.Type == BodyTokenType.EOF)
            {
                currentNode.Add(doc.CreateTextNode(stringBuilder.ToString()));
                return;
            }
            
            stringBuilder.Append(tokenizer.Current.Value);
            if (!tokenizer.MoveNext())
            {
                currentNode.Add(doc.CreateTextNode(stringBuilder.ToString()));
                return;
            }
            
            this.Literal(doc, currentNode, tokenizer, stringBuilder);
        }
        
        private void MokeyTail(CodeDocument doc, IEditableContainerNode currentNode, IEnumerator<IToken<BodyTokenType>> tokenizer, IToken<BodyTokenType> last)
        {
            var current = tokenizer.Current;
            var tokenType = current.Type;
            
            if (tokenType == BodyTokenType.MonkeyTail)
            {
                currentNode.Add(doc.CreateTextNode("@"));
                
                if (tokenizer.MoveNext())
                {
                    this.Reset(doc, currentNode, tokenizer);
                }
                
                return;
            }
            
            LinkedList<IToken<BodyTokenType>> agg = new LinkedList<IToken<BodyTokenType>>();
            
            agg.AddLast(last);
            agg.AddLast(current);
            
            if (tokenType == BodyTokenType.Asterisk)
            {
                if (!tokenizer.MoveNext())
                {
                    throw new CompilationException("Expecting End Comment '*@'");
                }
                
                this.Comment(doc, currentNode, tokenizer, agg);
                return;
            }
            
            if (tokenType == BodyTokenType.OpenBrace)
            {
                if (!tokenizer.MoveNext())
                {
                    throw new CompilationException("Expecting End Expression Escaped '}'");
                }
                
                this.ExpressionEscape(doc, currentNode, tokenizer, agg, false);
                return;
            }
            
            if (tokenType == BodyTokenType.LessThan)
            {
                if (!tokenizer.MoveNext())
                {
                    throw new CompilationException("Expecting End Expression Raw '>@'");
                }
                
                this.ExpressionRaw(doc, currentNode, tokenizer, agg);
                return;
            }
            
            if (tokenType == BodyTokenType.Ignore)
            {
                this.ExpressionEscape(doc, currentNode, tokenizer, agg, true);
                return;
            }
            
            currentNode.Add(doc.CreateTextNode("@"));
            
            // ALREADY MOVED
            this.Reset(doc, currentNode, tokenizer);
            
            return;
        }

        private void Comment(
            CodeDocument doc,
            IEditableContainerNode currentNode,
            IEnumerator<IToken<BodyTokenType>> tokenizer,
            LinkedList<IToken<BodyTokenType>> agg)
        {
            var current = tokenizer.Current;
            var tokenType = current.Type;
            
            if (!tokenizer.MoveNext())
            {
                throw new CompilationException("Expecting End Comment '*@'");
            }
            
            if (tokenType == BodyTokenType.Asterisk)
            {
                var next = tokenizer.Current;
                if (next.Type == BodyTokenType.MonkeyTail)
                {
                    currentNode.Add(doc.CreateComment(string.Concat(agg.Skip(2).Select(x => x.Value).ToArray())));
                    if (tokenizer.MoveNext())
                    {
                        this.Reset(doc, currentNode, tokenizer);
                    }
                    
                    return;
                }
            }
            
            agg.AddLast(current);
            this.Comment(doc, currentNode, tokenizer, agg);
        }
        
        private void ExpressionEscape(
            CodeDocument doc,
            IEditableContainerNode currentNode,
            IEnumerator<IToken<BodyTokenType>> tokenizer,
            LinkedList<IToken<BodyTokenType>> agg,
            bool simple)
        {
            var current = tokenizer.Current;
            var tokenType = current.Type;
            
            if (tokenType == BodyTokenType.Colon)
            {
                if (!tokenizer.MoveNext())
                {
                    throw new CompilationException("Expecting End Expression Escaped '}'");
                }
                
                var next = tokenizer.Current;
                var nextType = next.Type;
                
                if (tokenizer.Current.Type == BodyTokenType.Dash)
                {
                    if (!tokenizer.MoveNext())
                    {
                        throw new CompilationException("Expecting End Expression Escaped '}'");
                    }
                    
                    var nextNext = tokenizer.Current;
                    var nextNextType = nextNext.Type;
                    
                    if (tokenizer.Current.Type == BodyTokenType.GreaterThan)
                    {
                        if (!tokenizer.MoveNext())
                        {
                            throw new CompilationException("Expecting End Expression Escaped '}'");
                        }
                        
                        var nextNextNext = tokenizer.Current;
                        var nextNextNextType = nextNextNext.Type;
                        
                        agg.AddLast(new Token<BodyTokenType>
                            {
                                Value = "",
                                Type = BodyTokenType.MethodBreak
                            });
                        
                        var container = doc.CreateContainer(currentNode);
                        this.CapAgg(doc, currentNode, container, agg, false, false, false);
                        this.SwallowSpace(tokenizer);
                        this.Reset(doc, container, tokenizer);
                        
                        if (tokenizer == null || tokenizer.Current.Type != BodyTokenType.CloseBrace)
                        {
                            throw new CompilationException("Expecting End Expression Escaped '}'");
                        }
                        
                        if (tokenizer.MoveNext())
                        {
                            this.Reset(doc, currentNode, tokenizer);
                            return;
                        }
                        
                        //                        this.ExpressionMethodBody(doc, currentNode, tokenizer, agg, new LinkedList<IToken<BodyTokenType>>());
                    }
                    else
                    {
                        agg.AddLast(current);
                        agg.AddLast(next);
                        this.ExpressionEscape(doc, currentNode, tokenizer, agg, simple);
                        return;
                    }
                }
                else
                {
                    agg.AddLast(current);
                    this.ExpressionEscape(doc, currentNode, tokenizer, agg, simple);
                    return;
                }
            }
            
            if (tokenType == BodyTokenType.CloseBrace)
            {
                this.CapAgg(doc, currentNode, null, agg, false, false, simple);
                
                if (tokenizer.MoveNext())
                {
                    this.Reset(doc, currentNode, tokenizer);
                }
                
                return;
            }
            
            if (simple && tokenType == BodyTokenType.Ignore && !char.IsLetterOrDigit(current.Value[0]))
            {
                agg.AddLast(current);
                this.CapAgg(doc, currentNode, null, agg, false, false, simple);
                
                if (tokenizer.MoveNext())
                {
                    this.Reset(doc, currentNode, tokenizer);
                }
                
                return;
            }
            
            if (simple && tokenType != BodyTokenType.Ignore)
            {
                this.CapAgg(doc, currentNode, null, agg, false, false, simple);
                
                this.Reset(doc, currentNode, tokenizer);
                
                return;
            }
            
            if (!tokenizer.MoveNext())
            {
                throw new CompilationException("Expecting End Expression Escaped '}'");
            }
            
            agg.AddLast(current);
            this.ExpressionEscape(doc, currentNode, tokenizer, agg, simple);
        }
        
        private void ExpressionRaw(
            CodeDocument doc,
            IEditableContainerNode currentNode,
            IEnumerator<IToken<BodyTokenType>> tokenizer,
            LinkedList<IToken<BodyTokenType>> agg)
        {
            var current = tokenizer.Current;
            var tokenType = current.Type;
            
            if (!tokenizer.MoveNext())
            {
                throw new CompilationException("Expecting End Expression Raw '>@'");
            }
            
            if (tokenType == BodyTokenType.GreaterThan)
            {
                var next = tokenizer.Current;
                if (next.Type == BodyTokenType.MonkeyTail)
                {
                    this.CapAgg(doc, currentNode, null, agg, false, true, false);
                    
                    if (tokenizer.MoveNext())
                    {
                        this.Reset(doc, currentNode, tokenizer);
                    }
                    
                    return;
                }
            }
            
            agg.AddLast(current);
            this.ExpressionRaw(doc, currentNode, tokenizer, agg);
        }
        
        private void CapAgg(
            CodeDocument doc,
            IEditableContainerNode currentNode,
            IEditableContainerNode next,
            LinkedList<IToken<BodyTokenType>> agg,
            bool navigateUp,
            bool isRaw,
            bool isSimple)
        {
            Debug.WriteLine(currentNode.Parent);
            
            var processingInst = string.Concat(agg.Skip(2).Select(x => x.Value).ToArray());
            
            var trimmedProcIsnt = processingInst.Trim();
            
            var dest = navigateUp ? currentNode.Parent : currentNode;
            
            if (isSimple && !doc.Attributes.Any(x => string.Equals(x.Key, trimmedProcIsnt, StringComparison.Ordinal)))
            {
                var nodeAt = doc.CreateTextNode("@");
                (dest ?? doc).Add(nodeAt);
                
                var node = doc.CreateTextNode(processingInst);
                (dest ?? doc).Add(node);
                return;
            }
            
            var processingInstruct = doc.CreateProcessingInstruction(trimmedProcIsnt, isRaw, next);
            if (trimmedProcIsnt.Contains("\r") || trimmedProcIsnt.Contains("\n"))
            {
                string[] lines = processingInst.Split(new char[]
                    {
                        '\r',
                        '\n'
                    }, StringSplitOptions.RemoveEmptyEntries);
                
                lines = lines.Where(x => x != null)
                    .Where(x => !string.IsNullOrWhiteSpace(x.Trim()))
                    .ToArray();
                
                processingInstruct = doc.CreateProcessingInstruction(lines, isRaw, next);
            }
            
            (dest ?? doc).Add(processingInstruct);
            return;
        }

        public bool IsWhiteSpace(BodyTokenType type)
        {
            return type == BodyTokenType.WhiteSpace || type == BodyTokenType.CR || type == BodyTokenType.LF;
        }
    }
}

/*
 * 
 * 
            
//            if (currentToken.Type == BodyTokenType.CloseBrace && currentNode.Parent != null)
//            { 
//                // WHAT DO I DO HERE??
//                if (!tokenizer.MoveNext())
//                {
//                }
//                this.Reset(doc, currentNode.Parent, tokenizer);
//            }
 * 
        private void ExpressionMethodBody(
            CodeDocument doc,
            IEditableContainerNode currentNode,
            IEnumerator<IToken<BodyTokenType>> tokenizer,
            LinkedList<IToken<BodyTokenType>> preAgg,
            LinkedList<IToken<BodyTokenType>> agg)
        {
            var current = tokenizer.Current;
            var tokenType = current.Type;
            
            if (tokenType == BodyTokenType.CloseBrace)
            {
                var processingInst = string.Concat(preAgg.Skip(2).Select(x => x.Value).ToArray());
                
                var trimmedProcIsnt = processingInst.Trim();
                
                if (trimmedProcIsnt.Contains("\r") || trimmedProcIsnt.Contains("\n"))
                {
                    string[] lines = processingInst.Split(new char[] {
                        '\r',
                        '\n'
                    }, StringSplitOptions.RemoveEmptyEntries);
                    
                    lines = lines.Where(x => x != null)
                        .Where(x => !string.IsNullOrWhiteSpace(x.Trim()))
                        .ToArray();
                    
                    currentNode.Parent.Add(doc.CreateProcessingInstruction(lines, false, currentNode));
                } else
                {
                    currentNode.Parent.Add(doc.CreateProcessingInstruction(trimmedProcIsnt, false, currentNode));
                }
                
                if (tokenizer.MoveNext())
                {
                    this.Reset(doc, currentNode.Parent, tokenizer);
                }
                
                return;
            }
            
            if (!tokenizer.MoveNext())
            {
                throw new CompilationException("Expecting End Expression Escaped '}'");
            }
            
            agg.AddLast(current);
            this.ExpressionMethodBody(doc, currentNode, tokenizer, preAgg, agg);
        }
         */

