// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    
    public class HeaderLexer
    {
        private readonly string RequireKeyword = "require";
        
        private readonly string ReferenceKeyword = "reference";
        
        private readonly HeaderTokenizer headerTokenizer;

        private readonly List<string> requires = new List<string>();

        private readonly List<string> references = new List<string>();

        private readonly List<IParameter> parameters = new List<IParameter>();

        private bool hasLexed;
        
        public HeaderLexer(HeaderTokenizer headerTokenizer)
        {
            this.headerTokenizer = headerTokenizer;
        }
        
        public string[] Requires()
        {
            this.EnsureLexed();
            return this.requires.ToArray();
        }
        
        public string[] References()
        {
            this.EnsureLexed();
            return this.references.ToArray();
        }
        
        public IParameter[] Parameters()
        {
            this.EnsureLexed();
            return this.parameters.ToArray();
        }

        private void EnsureLexed()
        {
            if (!this.hasLexed)
            {
                this.Lex();
                this.hasLexed = true;
            }
        }

        private void Lex()
        {
            var results = this.headerTokenizer.Parse().GetEnumerator();
            if (results.MoveNext())
            {
                this.Reset(results);
            }
        }

        private void Statement(IEnumerator<IToken<HeaderTokenType>> tokenizer)
        {
            var currentToken = tokenizer.Current;
            if (currentToken.Type == HeaderTokenType.WhiteSpace)
            {
                tokenizer.MoveNext();
                this.Reset(tokenizer);
                return;
            }
            
            if (currentToken.Type == HeaderTokenType.MonkeyTail)
            {
                tokenizer.MoveNext();
                this.Parameter(tokenizer);
                return;
            }
            
            if (currentToken.Type == HeaderTokenType.NewLine)
            {
                tokenizer.MoveNext();
                this.Reset(tokenizer);
                return;
            }
            
            if (currentToken.Type == HeaderTokenType.Pound)
            {
                tokenizer.MoveNext();
                this.Pound(tokenizer);
                return;
            }
            
            if (currentToken.Type == HeaderTokenType.EOF)
            {
                return;
            }
            
            throw new CompilationException(currentToken.Type.ToString());
        }

        private void Pound(IEnumerator<IToken<HeaderTokenType>> tokenizer)
        {
            var newCurrent = this.ValidateCurrentAs(tokenizer, HeaderTokenType.Word);
            var newCurrentValue = newCurrent.Value;
                
            tokenizer.MoveNext();
               
            if (newCurrentValue != RequireKeyword && newCurrentValue != ReferenceKeyword)
            {
                var message = string.Format(
                                  "Expected Word, '{0}' or '{1}' got: '{2}'",
                                  RequireKeyword,
                                  ReferenceKeyword,
                                  newCurrentValue);
                    
                throw new CompilationException(message);
            }
                
            if (newCurrentValue == RequireKeyword)
            {
                this.Require(tokenizer);
            }
                
            if (newCurrentValue == ReferenceKeyword)
            {
                this.Reference(tokenizer);
            }
        }
        
        private void Require(IEnumerator<IToken<HeaderTokenType>> tokenizer)
        {
            this.SwallowSpace(tokenizer);
            var name = new StringBuilder();
            this.ReadTypeName(tokenizer, name);
            this.requires.Add(name.ToString());
            this.Reset(tokenizer);
        }

        private void Reference(IEnumerator<IToken<HeaderTokenType>> tokenizer)
        {
            this.SwallowSpace(tokenizer);
            var name = new StringBuilder();
            this.ReadTypeName(tokenizer, name);
            this.references.Add(name.ToString());
            this.Reset(tokenizer);
        }

        private void Parameter(IEnumerator<IToken<HeaderTokenType>> tokenizer)
        {
            this.SwallowSpace(tokenizer);
            
            var paramNameToken = this.ValidateCurrentAs(tokenizer, HeaderTokenType.Word);
            tokenizer.MoveNext();
            this.SwallowSpace(tokenizer);
            this.ValidateCurrentAs(tokenizer, HeaderTokenType.Colon);
            tokenizer.MoveNext();
            this.SwallowSpace(tokenizer);
            var paramTypeValue = this.ReadTypeName(tokenizer);
            
            this.parameters.Add(new Parameter {
                Name = paramNameToken.Value,
                TypeName = paramTypeValue
            });
            this.Reset(tokenizer);
        }
		
        private void Reset(IEnumerator<IToken<HeaderTokenType>> tokenizer)
        {
            this.Statement(tokenizer);
        }

        private void SwallowSpace(IEnumerator<IToken<HeaderTokenType>> tokenizer)
        {
            if (tokenizer.Current != null && tokenizer.Current.Type == HeaderTokenType.WhiteSpace)
            {
                tokenizer.MoveNext();
                this.SwallowSpace(tokenizer);
            }
        }
        
        private string ReadTypeName(IEnumerator<IToken<HeaderTokenType>> tokenizer)
        {
            var sb = new StringBuilder();
            this.ReadTypeName(tokenizer, sb);
            return sb.ToString();
        }
        
        private string[] ReadTypesName(IEnumerator<IToken<HeaderTokenType>> tokenizer)
        {
            var sb = new StringBuilder();
            var items = new List<string>();
            while (this.ReadTypeName(tokenizer, sb))
            {
                items.Add(sb.ToString());
                sb = new StringBuilder();
            }
            
            items.Add(sb.ToString());
            
            return items.ToArray();
        }
        
        private bool ReadTypeName(IEnumerator<IToken<HeaderTokenType>> tokenizer, StringBuilder name)
        {
            this.SwallowSpace(tokenizer);
            
            var current = this.ValidateCurrentAs(tokenizer, HeaderTokenType.Word);
            
            tokenizer.MoveNext();
            this.SwallowSpace(tokenizer);
            var next = tokenizer.Current;
            
            name.Append(current.Value);
            
            if (next.Type == HeaderTokenType.Comma)
            {
                tokenizer.MoveNext();
                return true;
            }
            
            if (next.Type == HeaderTokenType.Dot)
            {
                name.Append('.');
                tokenizer.MoveNext();
                this.SwallowSpace(tokenizer);
                this.ReadTypeName(tokenizer, name);
            }
            
            if (next.Type == HeaderTokenType.LessThan)
            {
                name.Append('<');
                tokenizer.MoveNext();
                this.SwallowSpace(tokenizer);
                var innerTypes = this.ReadTypesName(tokenizer);
                this.SwallowSpace(tokenizer);
                
                name.Append(string.Join(", ", innerTypes));
                
                this.ValidateCurrentAs(tokenizer, HeaderTokenType.GreaterThan);
                tokenizer.MoveNext();
                name.Append('>');
            }
            
//            
//            if (next.Type == HeaderTokenType.LessThan)
//            {
//                name.Append('>');
//                tokenizer.MoveNext();
//            }
            
            return false;
        }

        private IToken<HeaderTokenType> ValidateCurrentAs(
            IEnumerator<IToken<HeaderTokenType>> tokenizer,
            HeaderTokenType expected)
        { 
            var token = tokenizer.Current;
            
            if (token == null)
            {
                var message = string.Format("Expected {0}, got null", expected);
                throw new CompilationException(message);
            }
            
            if (token.Type != expected)
            {
                var message = string.Format("Expected {0}, got '{1}' with value: '{2}'", expected, token.Type, token.Value);
                throw new CompilationException(message);
            }
            
            return token;
        }
    }
}

