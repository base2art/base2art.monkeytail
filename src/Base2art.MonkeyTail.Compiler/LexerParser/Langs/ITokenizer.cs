// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser.Langs
{
    using System.Collections.Generic;
    using Base2art.MonkeyTail.IO;
    
    public interface ITokenizer<T>
    {
        IEnumerable<IToken<T>> Parse();
    }
}

