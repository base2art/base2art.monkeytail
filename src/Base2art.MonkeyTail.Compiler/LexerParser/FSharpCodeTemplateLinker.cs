// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using Base2art.MonkeyTail.Config;
    using Base2art.MonkeyTail.Diagnostics;
    using Base2art.MonkeyTail.IO;

    public class FSharpCodeTemplateLinker : ITemplateLinker
    {
        private readonly IViewsSettings settings;

        private readonly string[] pathsToLink;

        private readonly ILogger logger;

        private string workingDirectory;
        
        public FSharpCodeTemplateLinker(IViewsSettings settings, string workingDirectory, string[] pathsToLink, ILogger logger)
        {
            this.workingDirectory = workingDirectory;
            this.settings = settings;
            this.pathsToLink = pathsToLink;
            this.logger = logger;
        }

        public ISubPrcoessResult<string> Link()
        {
            var files = string.Join(
                            " ",
                            this.pathsToLink.Select(x => "\"" + x + "\""));

            var references = string.Join(" ", (this.settings.References ?? new IReference[0])
                                         .Select(x => "--reference:\"" + x.HintPath + "\""));
            var flags = string.Format("--nologo -o:\"{0}\" {1} --target:library", 
                            this.settings.OutputFile,
                            references);

            var args = flags + " " + files;

            Directory.CreateDirectory(Path.GetDirectoryName(this.settings.OutputFile));

            var rez = ProcessRunner.Execute(this.settings.LinkerPath, args, this.workingDirectory);
            if (this.logger != null)
            {
                string message = string.Format(
                                     "Running '{0}' {1}  With Args: {2}", 
                                     this.settings.LinkerPath,
                                     Environment.NewLine,                    
                                     args);
                
                this.logger.Log(message);
            }
            
            List<Error> errors = new List<Error>();
            List<Error> warnings = new List<Error>();
            LinkedList<string> buf = new LinkedList<string>();
            foreach (var line in rez.ErrorOutput.Split('\n').Reverse())
            {
                buf.AddFirst(line.Trim());
                
                if (line.ToLowerInvariant().Contains("error"))
                {
                    errors.Add(new Error(string.Join(" ", buf)));
                    continue;
                }
                else if (line.ToLowerInvariant().Contains("warning"))
                {
                    warnings.Add(new Error(string.Join(" ", buf)));
                    continue;
                }
            }
            
            var subProcessResult = new SubProcessResult<string> {
                ResultCode = rez.ExitCode,
                Errors = errors.ToArray(),
                Messages = FilterBy(rez, x => !x.Contains("warning") && !x.Contains("error"))
                                           .Select(x => x.Message)
                                           .Union(rez.StandardOutput.Trim().Split('\n').Select(x => x.Trim()))
                                           .ToArray(),
                ReturnValue = this.settings.OutputFile,
                Warnings = warnings.ToArray(),
            };
            
            if (rez.ExitCode == 0 && !this.settings.SkipSecondaryOutputFile)
            {
                try
                {
                    var @out = Path.Combine(this.workingDirectory, this.settings.SecondaryOutputFile);
                    Directory.CreateDirectory(Path.GetDirectoryName(@out));
                    File.Copy(
                        Path.Combine(this.workingDirectory, this.settings.OutputFile),
                        @out,
                        true);
                }
                catch (IOException exception)
                {
                    warnings.Add(new Error(exception.Message));
                    subProcessResult.Warnings = warnings.ToArray();
                }
            }

            return subProcessResult;
        }

        private static IError[] FilterBy(ProcessEndInfo rez, Func<string, bool> filter)
        {
            return rez.ErrorOutput.Split('\n')
                .Where(x => filter(x.ToLowerInvariant()))
                .Select(x => new Error(x.Trim())).ToArray();
        }
    }
}

