// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.LexerParser
{
    using System;
    using System.Runtime.Serialization;

    public class CompilationException : Exception, ISerializable
    {
        public CompilationException()
        {
        }

        public CompilationException(string message)
            : base(message)
        {
        }

        public CompilationException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected CompilationException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

