
namespace Base2art.MonkeyTail.LexerParser
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    
    public static class AssemblerExtender
    {
        public static void AddInEscapeAndRawMethods(this StringBuilder sb, IFileType fileType)
        {
//            var type = fileType.ToString("G");
//            var name = fileType.ToString("G").ToLowerInvariant();
//            sb.AppendFormatLine("         let Escape(text:System.Object) =");
//            sb.AppendFormatLine("                                match text with");
//            sb.AppendFormatLine("                                            | :? System.String as x -> this.Format.Escape(x)");
//            sb.AppendFormatLine("                                            | :? {0} as {1} -> {1}", type, name);
//            sb.AppendFormatLine("                                            | :? System.Collections.Generic.IEnumerable<System.String> as x -> List.ofSeq(x) |> List.map(fun y -> this.Format.Escape(y)) |> List.fold(fun runningSum nextVal -> runningSum.Append(nextVal)) (new {0}(new System.Text.StringBuilder()))", type);
//            sb.AppendFormatLine("                                            | :? System.Collections.Generic.IEnumerable<{0}> as x -> List.ofSeq(x) |> List.fold(fun runningSum nextVal -> runningSum.Append(nextVal)) (new {0}(new System.Text.StringBuilder()))", type);
//            sb.AppendFormatLine("                                            | line -> this.Format.Escape(line.ToString())");
//            sb.AppendFormatLine("         let Raw(text:System.Object) =");
//            sb.AppendFormatLine("                                match text with");
//            sb.AppendFormatLine("                                            | :? System.String as x -> this.Format.Raw(x)");
//            sb.AppendFormatLine("                                            | :? {0} as {1} -> {1}", type, name);
//            sb.AppendFormatLine("                                            | :? System.Collections.Generic.IEnumerable<System.String> as x -> List.ofSeq(x) |> List.map(fun y -> this.Format.Raw(y)) |> List.fold(fun runningSum nextVal -> runningSum.Append(nextVal)) (new {0}(new System.Text.StringBuilder()))", type);
//            sb.AppendFormatLine("                                            | :? System.Collections.Generic.IEnumerable<{0}> as x -> List.ofSeq(x) |> List.fold(fun runningSum nextVal -> runningSum.Append(nextVal)) (new {0}(new System.Text.StringBuilder()))", type);
//            sb.AppendFormatLine("                                            | line -> this.Format.Raw(line.ToString())");
            sb.AppendLine();
        }
        
        public static string ParameterList(this MethodInvokation mi, bool includeChildren = false)
        {
            var x = new LinkedList<string>();
            ParameterList(mi, x);
            
            if (includeChildren)
            {
                foreach (var element in  mi.ChildrenParamsMax())
                {
                    x.AddLast(element);
                }
            }
            
            return string.Join(", ", x);
        }

        public static string ParameterListInvocation(this MethodInvokation mi, bool includeChildren = false)
        {
            var x = new LinkedList<string>();
            ParameterListInvocation(mi, x);
            
            if (includeChildren)
            {
                foreach (var element in mi.ChildrenParamsMax())
                {
                    x.AddLast(element);
                }
            }
            
            return string.Join(", ", x);
        }
        
        public static string[] ChildrenParamsMax(this MethodInvokation mi)
        {
            var additionalParams = new string[0];
            foreach (var element in mi.Items)
            {
                if (element.AnonymousChildParams.Length > additionalParams.Length)
                {
                    additionalParams = element.AnonymousChildParams;
                }
            }
            
            return additionalParams;
        }
        
        private static void ParameterList(MethodInvokation mi, LinkedList<string> parms)
        {
            if (mi.ParentNode != null)
            {
                ParameterList(mi.ParentNode, parms);
            }
            
            if (mi.Node != null)
            {
                foreach (var i in mi.Node.Attributes.Select(c => c.Key + ":" + c.Value))
                {
                    parms.AddLast(i);
                }
            }
            
            foreach (var param in mi.AnonymousChildParams)
            {
                parms.AddLast(param);
            }
        }

        private static void ParameterListInvocation(MethodInvokation mi, LinkedList<string> parms)
        {
            if (mi.ParentNode != null)
            {
                ParameterListInvocation(mi.ParentNode, parms);
            }
            
            if (mi.Node != null)
            {
                foreach (var i in mi.Node.Attributes.Select(c => c.Key))
                {
                    parms.AddLast(i);
                }
            }
            
            foreach (var param in mi.AnonymousChildParams)
            {
                parms.AddLast(param);
            }
        }
    }
}

