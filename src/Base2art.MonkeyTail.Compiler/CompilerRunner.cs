// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    using System;
    using System.IO;

    using Base2art.MonkeyTail.Config;
    using Base2art.MonkeyTail.Diagnostics;

    public static class CompilerRunner
    {
        public static ICompiler StartWatching(
            ICodeType codeType,
            string directoryToWatch,
            IViewsSettings settings,
            IMessenger messenger,
            ILogger logger)
        {
            var app_views = Path.Combine(directoryToWatch, settings.RelativeSourceDirectory);
            var root = directoryToWatch;
            if (Directory.Exists(app_views))
            {
                directoryToWatch = app_views;
            }

            var program = new Compiler(
                codeType,
                root,
                directoryToWatch,
                settings.RelativeIntermediateDirectory,
                settings,
                messenger,
                logger);
            program.Run();
            return program;
        }

        public static IViewsSettings GetBuildSettings(
            string rootDir, string relativeProjectPath, IJsonDeserializer serializer, bool writeFileIfNotExist)
        {
            var buildYamlFile = Path.Combine(rootDir, relativeProjectPath);

            Directory.CreateDirectory(Path.GetDirectoryName(buildYamlFile));

            var projectsDir = Path.Combine(rootDir, "project");

            if (File.Exists(buildYamlFile))
            {
                
                var fileStream = File.ReadAllText(buildYamlFile);
                var buildSettings = serializer.Deserialize<ViewsSettings>(fileStream) ?? new ViewsSettings();
                return new ViewsSettingsWrapper(buildSettings, rootDir, projectsDir);
            }
            
            var settings = new ViewsSettingsWrapper(
                new ViewsSettings(),
                rootDir,
                projectsDir);
            
            if (writeFileIfNotExist)
            {
                var text = serializer.Serialize(settings);
                File.WriteAllText(buildYamlFile, text);
            }
            
            return settings;
        }
    }
}

