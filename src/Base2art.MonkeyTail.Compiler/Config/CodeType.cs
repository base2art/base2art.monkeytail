// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Config
{
    using System;
    public class CodeType : ICodeType
    {
        public CodeType()
        {
        }
	    
        public CodeType(string value)
        {
            var parts = (value ?? string.Empty).Split(":".ToCharArray(), 2, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2)
            {
                throw new ArgumentException("CodeType must be in the format `Name:Extension`");
            }
            
            this.Name = parts[0];
            this.Extension = parts[1];
        }
	    
        public string Name { get; set; }

        public string Extension { get; set; }
    }
}

