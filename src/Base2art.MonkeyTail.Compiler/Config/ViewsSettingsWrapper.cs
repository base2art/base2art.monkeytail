// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Config
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    public class ViewsSettingsWrapper : IViewsSettings
    {
        private static string BackingFsc;

        private readonly IViewsSettings viewSettings;

        private readonly string rootDir;

        private readonly string projectsDir;
        
        public ViewsSettingsWrapper(IViewsSettings viewSettings, string rootDir, string projectsDir)
        {
            this.viewSettings = viewSettings ?? new ViewsSettings();
            this.rootDir = rootDir;
            this.projectsDir = projectsDir;
        }
        
        public static string Fsc
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(BackingFsc))
                {
                    return BackingFsc;
                }
                
                var x86 = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
                var x64 = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles);
                
                var fscPaths = new string[]
                {
                    Path.Combine(x86, "Microsoft F#", "v4.0", "Fsc.exe"),
                    Path.Combine(x86, "Microsoft SDKs", "F#", "4.0", "Framework", "v4.0", "Fsc.exe"),
                    Path.Combine(x64, "Microsoft F#", "v4.0", "Fsc.exe"),
                    Path.Combine(x64, "Microsoft SDKs", "F#", "4.0", "Framework", "v4.0", "Fsc.exe")
                };
                
                foreach (var fscPath in fscPaths)
                {
                    if (File.Exists(fscPath))
                    {
                        BackingFsc = fscPath;
                        return BackingFsc;
                    }
                }
                
                throw new FileNotFoundException("FSC Not Installed");
            }
        }

        public IReference[] References
        {
            get
            {
                var references = this.viewSettings.References ?? new IReference[0];
                const string MonkeyTailName = "Base2art.MonkeyTail.Api";
                if (references.All(x => x.Name != MonkeyTailName))
                {
                    var list = new List<IReference>(references);
                    
                    var hintPath = Path.Combine(this.rootDir, "lib", "Base2art.MonkeyTail.Api.1.0.0.0", "lib", "net", MonkeyTailName + ".dll");
                    
                    if (!File.Exists(hintPath))
                    {
                        hintPath = Path.Combine(this.projectsDir, "Packages", "Base2art.MonkeyTail.Api.1.0.0.0", "lib", "net", MonkeyTailName + ".dll");
                        
                        if (!File.Exists(hintPath))
                        {
                            hintPath = Path.Combine(
                                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                "base2art", 
                                "Base2art.MonkeyTail.Compiler.Console", 
                                "tools",
                                MonkeyTailName + ".dll");
                        }
                    }
                    
                    list.Add(new Reference
                             {
                                 Name = MonkeyTailName,
                                 HintPath = hintPath
                             });
                    references = list.ToArray();
                }

                return references;
            }
        }

        public string[] Imports
        {
            get
            {
                return this.viewSettings.Imports ?? new string[0];
            }
        }

        public IFileType[] OutputFormats
        {
            get
            {
                return this.viewSettings.OutputFormats ?? new IFileType[0];
            }
        }

        public string LinkerPath
        {
            get
            {
                var linkerPath = this.viewSettings.LinkerPath;
                if (string.IsNullOrWhiteSpace(linkerPath))
                {
                    return Fsc;
                }

                return linkerPath;
            }
        }

        public string OutputFile
        {
            get
            {
                var outputFile = this.viewSettings.OutputFile;
                if (string.IsNullOrWhiteSpace(outputFile))
                {
                    return Path.Combine(this.projectsDir, "lib", "appViews.dll");
                }

                return outputFile;
            }
        }

        public bool SkipSecondaryOutputFile
        {
            get
            {
                return this.viewSettings.SkipSecondaryOutputFile;
            }
        }

        public string SecondaryOutputFile
        {
            get
            {
                var outputFile = this.viewSettings.SecondaryOutputFile;
                if (string.IsNullOrWhiteSpace(outputFile))
                {
                    return Path.Combine(this.rootDir, "target", "bin", "appViews.dll");
                }

                return outputFile;
            }
        }

        public string RelativeIntermediateDirectory
        {
            get
            {
                var relativeIntermediateDirectory = this.viewSettings.RelativeIntermediateDirectory;
                if (string.IsNullOrWhiteSpace(relativeIntermediateDirectory))
                {
                    return Path.Combine(this.projectsDir, "app_views");
                }

                return relativeIntermediateDirectory;
            }
        }

        public string RelativeSourceDirectory
        {
            get
            {
                var relativeSourceDirectory = this.viewSettings.RelativeSourceDirectory;
                if (string.IsNullOrWhiteSpace(relativeSourceDirectory))
                {
                    return Path.Combine(this.rootDir, "App/Views");
                }

                return relativeSourceDirectory;
            }
        }
    }
}

