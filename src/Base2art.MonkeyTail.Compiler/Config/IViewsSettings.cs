// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Config
{
    public interface IViewsSettings
    {
        IReference[] References { get; }

        string[] Imports { get; }

        string LinkerPath { get; }

        string OutputFile { get; }

        bool SkipSecondaryOutputFile { get; }

        string SecondaryOutputFile { get; }

        string RelativeIntermediateDirectory { get; }

        string RelativeSourceDirectory { get; }

        IFileType[] OutputFormats { get; }
    }
}

