// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Config
{
    public interface IReference
    {
        string Name { get; }

        string HintPath { get; }
    }
}

