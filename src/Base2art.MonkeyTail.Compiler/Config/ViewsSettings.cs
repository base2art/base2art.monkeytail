// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail.Config
{
    public class ViewsSettings : IViewsSettings
    {
        private FileType[] outputFormats;
        
        private Reference[] references;
        
        public FileType[] OutputFormats 
        {
            get { return (this.outputFormats = this.outputFormats ?? new FileType[0]); }
            
            set { this.outputFormats = value; }
        }
        
        public Reference[] References 
        {
            get { return (this.references = this.references ?? new Reference[0]); }
            
            set { this.references = value; }
        }

        public string[] Imports { get; set; }

        public string LinkerPath { get; set; }

        public string OutputFile { get; set; }

        public bool SkipSecondaryOutputFile { get; set; }

        public string SecondaryOutputFile { get; set; }

        public string RelativeIntermediateDirectory { get; set; }

        public string RelativeSourceDirectory { get; set; }

        IReference[] IViewsSettings.References
        {
            get { return this.References; }
        }

        IFileType[] IViewsSettings.OutputFormats
        {
            get { return this.OutputFormats; }
        }
    }
}

