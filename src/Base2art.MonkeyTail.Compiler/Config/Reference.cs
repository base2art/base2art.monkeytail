// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

using System;
namespace Base2art.MonkeyTail.Config
{
    public class Reference : IReference
    {
        public Reference()
        {
        }
        
        public Reference(string value)
        {
            var parts = (value ?? string.Empty).Split(":".ToCharArray(), 2, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length != 2) 
            {
                throw new ArgumentException("Reference must be in the format `Name:HintPath`");
            }
            
            this.Name = parts[0];
            this.HintPath = parts[1];
        }
        
        public string Name { get; set; }

        public string HintPath { get; set; }
    }
}

