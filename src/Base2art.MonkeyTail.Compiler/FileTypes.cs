// <copyright company="Base2art">
// Copyright (c) 2014 All Rights Reserved
// </copyright>
// <author>Scott Youngblut</author>

namespace Base2art.MonkeyTail
{
    public static class FileTypes
    {
        public readonly static IFileType Html = new InnerFileType("Html", "html");
        public readonly static IFileType Txt = new InnerFileType("Txt", "txt");
        public readonly static IFileType Xml = new InnerFileType("Xml", "xml");
        public readonly static IFileType Javascript = new InnerFileType("Javascript", "js");
        
        public static IFileType[] KnownValues
        {
            get
            {
                return new []
                {
                    Html,
                    Txt,
                    Xml,
                    Javascript
                };
            }
        }
        
        private class InnerFileType : IFileType
        {
            private readonly string name;
        
            private readonly string searchPattern;
        
            public InnerFileType(string name, string searchPattern)
            {
                this.name = name;
                this.searchPattern = searchPattern;
            }

            public string Extension
            {
                get { return this.searchPattern; }
            }

            public string Name
            {
                get { return name; }
            }
            
            public override string ToString()
            {
                return this.name;
            }
        }
    }
}

