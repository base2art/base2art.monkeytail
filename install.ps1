﻿# =====================================================================
# Copyright 2015 - Present Base2art, LLC, and the
# original authors/contributors from Base2art
# at https://github.com/Base2art/
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =====================================================================

# Based on file here: https://github.com/chocolatey/chocolatey.org

$productName = "base2art.monkeytail.compiler.console";
$productVersion = "1.0.0.0";
$commandName = "monkeytail";


$url = "https://nuget.base2art.com/api/v2/package/$productName/$productVersion"

$powershellProdDir = Split-Path $profile

$installDir = $env:LocalAppData
if ($installDir -eq $null) {
  $installDir = $env:USERPROFILE
}

$b2aDir = Join-Path $installDir "base2art"
$monkeytail = Join-Path $b2aDir $productName
$packagesDir = Join-Path $b2aDir ".nuget-cache"

# CREATE DIRECTORY AND SUB DIRS
if (![System.IO.Directory]::Exists($monkeytail)) {
  [System.IO.Directory]::CreateDirectory($monkeytail)
}

if (![System.IO.Directory]::Exists($packagesDir)) {
  [System.IO.Directory]::CreateDirectory($packagesDir)
}

if (![System.IO.Directory]::Exists($powershellProdDir)) {
  [System.IO.Directory]::CreateDirectory($powershellProdDir)
}

$packagePath = Join-Path $b2aDir "$productName.$productVersion.nupkg.zip"

function Download-File {
param (
  [string]$url,
  [string]$file
 )
  if (-not (Test-Path $file)) {
    Write-Output "Downloading $url to $file"
    $downloader = new-object System.Net.WebClient
    $downloader.DownloadFile($url, $file)
  }
}

# Download the package
Download-File $url $packagePath

# unzip the package
Write-Output "Extracting $packagePath to $monkeytail..."
$shellApplication = new-object -com shell.application
$zipPackage = $shellApplication.Namespace($packagePath)
$destinationFolder = $shellApplication.Namespace($monkeytail)
$destinationFolder.CopyHere($zipPackage.Items(),0x10)

$toolsFolder = Join-Path $monkeytail "tools"


Write-Output 'Ensuring commands are on the path'
$installVariableName = "monkeytail"
$productPath = [Environment]::GetEnvironmentVariable($installVariableName)
if ($productPath -eq $null -or $productPath -eq '') {
  $productPath = $toolsFolder
}


$oldExe = Join-Path $toolsFolder "Base2art.MonkeyTail.Compiler.Console.exe"
$newExe = Join-Path $toolsFolder "MonkeyTail.exe"
Copy-Item -Force $oldExe $newExe

#Upate ProfileDir
if ($($env:Path).ToLower().Contains($($toolsFolder).ToLower()) -eq $false) {
  $old = [Environment]::GetEnvironmentVariable('Path',[System.EnvironmentVariableTarget]::Machine);
  if (-Not $old.EndsWith(";")) {
    $old = "$old;"
  }
  
  $old += "$toolsFolder;"
  $env:Path = $old
}


$outputText = @"

`$toolsFolder = "$toolsFolder";


if (`$(`$env:Path).ToLower().Contains(`$(`$toolsFolder).ToLower()) -eq `$false) {
  `$old = [Environment]::GetEnvironmentVariable('Path',[System.EnvironmentVariableTarget]::Machine);
  if (-Not `$old.EndsWith(";")) {
    `$old = "`$old;"
  }
  
  `$old += "`$toolsFolder;";
  `$env:Path = `$old;
}

"@


Out-File -Append -FilePath $profile -InputObject $outputText

