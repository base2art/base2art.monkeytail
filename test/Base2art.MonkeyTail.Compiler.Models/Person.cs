namespace Base2art.MonkeyTail.Compiler.Models
{
    public class Person
    {
        public string Name { get; set; }
        public byte Height { get; set; }
    }
}

