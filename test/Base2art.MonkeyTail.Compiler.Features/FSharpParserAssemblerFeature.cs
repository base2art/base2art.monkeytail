namespace Base2art.MonkeyTail.Compiler.Features
{
    using System.IO;

    using Base2art.MonkeyTail.CodeDom;
    using Base2art.MonkeyTail.CodeDom.Impl;
    using Base2art.MonkeyTail.LexerParser;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class FSharpParserAssemblerFeature
    {
        [Test]
        public void ShouldAssembleNoParameters()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument(string.Empty, "SOME_NAME", FileTypes.Html);

            doc.Add(doc.CreateTextNode("Start"));
            doc.Add(doc.CreateProcessingInstruction("count * 2", false));
            doc.Add(doc.CreateTextNode("End"));
            var actual = fsp.Assemble(doc).ReturnValue.Trim();

            var expected = @"namespace Views.Html

open Base2art.MonkeyTail
open Base2art.MonkeyTail.Api
open Base2art.MonkeyTail.ContentTypes

type SOME_NAME() =

  inherit ExpressiveTemplateBase<Html, IFormat<Html>>(new HtmlFormat())

  interface ITemplate0<Html> with
    member this.Render() = this.Render()
  end

  with
    static member Apply() = (new SOME_NAME()).Render()
    member this.Render() = 

         this.Raw(this.Text0).Append(this.Escape(this.ProcInst1())).Append(this.Raw(this.Text2))

    member this.Text0 = ""Start""
    member this.ProcInst1() = count * 2
    member this.Text2 = ""End""";
            actual.Should().Be(expected);
        }

        [Test]
        public void ShouldAssembleMultipleParameters()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument(string.Empty, "SOME_NAME", FileTypes.Html);

            doc.SetAttribute("x", "string");
            doc.SetAttribute("y", "string");

            doc.Add(doc.CreateTextNode("Start"));
            doc.Add(doc.CreateProcessingInstruction("count * 2", false));
            doc.Add(doc.CreateTextNode("End"));
            var actual = fsp.Assemble(doc).ReturnValue.Trim();

            var expected = @"namespace Views.Html

open Base2art.MonkeyTail
open Base2art.MonkeyTail.Api
open Base2art.MonkeyTail.ContentTypes

type SOME_NAME() =

  inherit ExpressiveTemplateBase<Html, IFormat<Html>>(new HtmlFormat())

  interface ITemplate2<string, string, Html> with
    member this.Render(x:string, y:string) = this.Render(x, y)
  end

  with
    static member Apply(x:string, y:string) = (new SOME_NAME()).Render(x, y)
    member this.Render(x:string, y:string) = 

         this.Raw(this.Text0).Append(this.Escape(this.ProcInst1(x, y))).Append(this.Raw(this.Text2))

    member this.Text0 = ""Start""
    member this.ProcInst1(x:string, y:string) = count * 2
    member this.Text2 = ""End""";
            actual.Should().Be(expected);
        }

        [Test]
        public void ShouldAssemble()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument(string.Empty, "SOME_NAME", FileTypes.Html);
            doc.SetAttribute("x", "string");

            doc.Add(doc.CreateTextNode("Start"));
            doc.Add(doc.CreateProcessingInstruction("count * 2", false));
            doc.Add(doc.CreateTextNode("End"));
            var asmed = fsp.Assemble(doc).ReturnValue.Trim();
            asmed.Should().Contain(this.Header("SOME_NAME"));
            
            var hdr = this.Header("SOME_NAME");
            asmed.Should().StartWith(hdr);
            asmed = asmed.Substring(hdr.Length);

            var expected =   @"

         this.Raw(this.Text0).Append(this.Escape(this.ProcInst1(x))).Append(this.Raw(this.Text2))

    member this.Text0 = ""Start""
    member this.ProcInst1(x:string) = count * 2
    member this.Text2 = ""End""";
            asmed.Should().Be(expected);
        }

        [Test]
        public void ShouldAssembleWithNamespaceAndClassName()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument("User", "Index", FileTypes.Html);
            doc.SetAttribute("x", "string");

            doc.Add(doc.CreateTextNode("\"Start\""));
            doc.Add(doc.CreateProcessingInstruction("count * 2", false));
            doc.Add(doc.CreateTextNode("End"));
            var actual = fsp.Assemble(doc).ReturnValue.Trim();
            actual.Should().Contain(this.Header("User", "Index", "x", "string"));
            actual.Should().Be(this.Header("User", "Index", "x", "string") + @"

         this.Raw(this.Text0).Append(this.Escape(this.ProcInst1(x))).Append(this.Raw(this.Text2))

    member this.Text0 = ""\""Start\""""
    member this.ProcInst1(x:string) = count * 2
    member this.Text2 = ""End""");
        }

        [Test]
        public void ShouldAssembleWithComments()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument("User", "Index", FileTypes.Html);
            doc.SetAttribute("x", "string");

            doc.Add(doc.CreateTextNode("Start"));
            doc.Add(doc.CreateComment("Comment 1"));
            fsp.Assemble(doc).ReturnValue.Trim().Should().Be(this.Header("User", "Index", "x", "string") + @"

         this.Raw(this.Text0)

    member this.Text0 = ""Start""");
        }

        [Test]
        public void ShouldAssembleWithParameters()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument("User", "Index", FileTypes.Html);
            doc.SetAttribute("count", "int");

            fsp.Assemble(doc).ReturnValue.Trim().Should().Be(this.Header("User", "Index", "count", "int") + @"

         Raw("""")");
        }

        [Test]
        public void ShouldAssembleOnlyExpression()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument("User", "Index", FileTypes.Html);
            doc.SetAttribute("count", "int");
            doc.Add(doc.CreateProcessingInstruction("count * 2", false));

            var expected = this.Header("User", "Index", "count", "int") + @"

         this.Escape(this.ProcInst0(count))

    member this.ProcInst0(count:int) = count * 2";
            fsp.Assemble(doc).ReturnValue.Trim().Should().Be(expected);
        }

        [Test]
        public void ShouldAssembleText()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument("User", "Index", FileTypes.Txt);
            doc.SetAttribute("count", "int");
            doc.Add(doc.CreateProcessingInstruction("count * 2", false));

            fsp.Assemble(doc).ReturnValue.Trim().Should().Be(this.Header("User", "Index", "count", "int", "Txt").Replace("Html", "Txt").Replace("html", "txt") + @"

         this.Escape(this.ProcInst0(count))

    member this.ProcInst0(count:int) = count * 2");
        }

        [Test]
        public void ShouldAssembleRawHtml()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument("User", "Index", FileTypes.Html);
            doc.SetAttribute("count", "int");
            doc.Add(doc.CreateProcessingInstruction("count * 2", true));

            fsp.Assemble(doc).ReturnValue.Trim().Should().Be(this.Header("User", "Index", "count", "int", "Html") + @"

         this.Raw(this.ProcInst0(count))

    member this.ProcInst0(count:int) = count * 2");
        }

        [Test]
        public void ShouldAssembleInnerProcessingInstruction()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument("User2", "Index2", FileTypes.Html);
            doc.SetAttribute("items", "System.Collections.Generic.List<System.Tuple<System.String, int>>");
            var container = new CodeContainer(doc);
            container.Add(doc.CreateTextNode("<li>"));
            container.Add(doc.CreateProcessingInstruction("$_.Item1", false));
            container.Add(doc.CreateTextNode("</li>"));
            doc.Add(doc.CreateProcessingInstruction("List.ofSeq(items) |> List.map($func)", false, container));
            
            var asmed = fsp.Assemble(doc).ReturnValue.Trim();
            var hdr = this.Header("User2", "Index2", "items", "System.Collections.Generic.List<System.Tuple<System.String, int>>", "Html");
            asmed.Should().StartWith(hdr);
            asmed = asmed.Substring(hdr.Length);
            asmed.TrimEnd().Should().Be(@"

         this.Escape(this.ProcInst0(items))

    member this.ProcInst0(items:System.Collections.Generic.List<System.Tuple<System.String, int>>) = List.ofSeq(items) |> List.map(fun (v_0_1_p_0) -> this.ProcInst0Agg(items, v_0_1_p_0))
    member this.ProcInst0Agg(items:System.Collections.Generic.List<System.Tuple<System.String, int>>, v_0_1_p_0) = 

         this.Raw(this.Text0_0).Append(this.Escape(this.ProcInst0_1(items, v_0_1_p_0))).Append(this.Raw(this.Text0_2))

    member this.Text0_0 = ""<li>""
    member this.ProcInst0_1(items:System.Collections.Generic.List<System.Tuple<System.String, int>>, v_0_1_p_0) = v_0_1_p_0.Item1
    member this.Text0_2 = ""</li>""");

            File.WriteAllText(DirectoryProvider.TemporaryFSharpFile2, fsp.Assemble(doc).ReturnValue);
        }

        [Test]
        public void ShouldCompileTempFile()
        {
            var fsp = new FSharpTemplateParser();

            IDocument doc = new CodeDocument("User", "Index", FileTypes.Html);
            doc.SetAttribute("count", "int");

            doc.Add(doc.CreateTextNode("\"Start\""));
            doc.Add(doc.CreateComment("THIS IS MY COMMENT"));
            doc.Add(doc.CreateProcessingInstruction("count * 2", false));
            doc.Add(doc.CreateTextNode("End"));

            File.WriteAllText(DirectoryProvider.TemporaryFSharpFile, fsp.Assemble(doc).ReturnValue);
        }

        private string Header(string typeName)
        {
            return string.Format(@"namespace Views.Html

open Base2art.MonkeyTail
open Base2art.MonkeyTail.Api
open Base2art.MonkeyTail.ContentTypes

type {0}() =

  inherit ExpressiveTemplateBase<Html, IFormat<Html>>(new HtmlFormat())

  interface ITemplate1<string, Html> with
    member this.Render(x:string) = this.Render(x)
  end

  with
    static member Apply(x:string) = (new {0}()).Render(x)
    member this.Render(x:string) = ",
                typeName);

        }

        private string Header(string ns, string typeName, string parameterName, string parameterType, string resultType = "Html")
        {
            return string.Format(@"namespace Views.Html.{1}

open Base2art.MonkeyTail
open Base2art.MonkeyTail.Api
open Base2art.MonkeyTail.ContentTypes

type {0}() =

  inherit ExpressiveTemplateBase<{4}, IFormat<{4}>>(new {4}Format())

  interface ITemplate1<{3}, {4}> with
    member this.Render({2}:{3}) = this.Render({2})
  end

  with
    static member Apply({2}:{3}) = (new {0}()).Render({2})
    member this.Render({2}:{3}) = ",
                typeName, ns, parameterName, parameterType, resultType);

        }
    }
}

