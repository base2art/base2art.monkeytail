namespace Base2art.MonkeyTail.Compiler.Features
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;

    using Base2art.MonkeyTail.CodeDom;
    using Base2art.MonkeyTail.LexerParser;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class FSharpParserLexerFeature
    {
        [Test]
        public void ShouldParseFSharpVariableTextTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"simple-expression.fs.html";
            using (var stream = this.Read(Fname))
            {
                var output = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                output.ChildNodes.First().As<IProcessingInstruction>().Value[0].Should().Be("count");
            }
        }
        
        [Test]
        public void ShouldParseFSharpVariable2TextTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"simple-expression-2.fs.html";
            using (var stream = this.Read(Fname))
            {
                var output = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                output.ChildNodes.Skip(0).First().As<ITextNode>().Text.Should().Be("<h2>");
                output.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Value[0].Should().Be("count");
                output.ChildNodes.Skip(2).First().As<ITextNode>().Text.Trim().Should().Be("</h2>");
            }
        }
        
        [Test]
        public void ShouldParseFSharpLiteralTextTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"literal-text.fs.html";
            using (var stream = this.Read(Fname))
            {
                var output = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                output.ChildNodes.First().As<ITextNode>().Text.Should().Be("Some Text");
            }
        }

        [Test]
        public void ShouldParseFSharpLiteralMonkeyTailTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"literal-monkeytail.fs.html";
            using (var stream = this.Read(Fname))
            {
                var output = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                output.ChildNodes.First().As<ITextNode>().Text.Should().Be("scott");
                output.ChildNodes.Skip(1).First().As<ITextNode>().Text.Should().Be("@");
                output.ChildNodes.Skip(2).First().As<ITextNode>().Text.Should().Be("base2art.com");
            }
        }

        [Test]
        public void ShouldParseFSharpLiteralMonkeyTailNoFollowUpTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"literal-monkeytail-no-follow-up.fs.html";
            using (var stream = this.Read(Fname))
            {
                var output = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                output.ChildNodes.First().As<ITextNode>().Text.Should().Be("scott");
//                output.ChildNodes.Skip(1).First().As<ITextNode>().Text.Should().Be("@base2art.com");
                
                output.ChildNodes.Skip(1).First().As<ITextNode>().Text.Should().Be("@");
                output.ChildNodes.Skip(2).First().As<ITextNode>().Text.Should().Be("base2art.");
                output.ChildNodes.Skip(3).First().As<ITextNode>().Text.Should().Be("com");
            }
        }

        [Test]
        public void ShouldParseFSharpExpressionTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"expression.fs.html";
            using (var stream = this.Read(Fname))
            {
                var doc = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                doc.ChildNodes.First().As<ITextNode>().Text.Should().Be("Some Text ");
                doc.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Value[0].Should().Be("User.Name");
                doc.ChildNodes.Skip(2).First().As<ITextNode>().Text.Should().Be(" ");
                doc.ChildNodes.Skip(3).First().As<IProcessingInstruction>().Value[0].Should().Be("count");
            }
        }

        [Test]
        public void ShouldParseFSharpWithParametersTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"parameters.fs.html";
            using (var stream = this.Read(Fname))
            {
                var doc = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                doc.Attributes.Count().Should().Be(2);
                doc.Attributes.First().Key.Should().Be("user");
                doc.Attributes.First().Value.Should().Be("Base2art.NS.IUser");
                doc.Attributes.Skip(1).First().Key.Should().Be("count");
                doc.Attributes.Skip(1).First().Value.Should().Be("int");

                doc.ChildNodes.First().As<ITextNode>().Text.Should().Be("Some Text ");
                doc.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Value[0].Should().Be("user.Name");
                doc.ChildNodes.Skip(2).First().As<ITextNode>().Text.Should().Be(" ");
                doc.ChildNodes.Skip(3).First().As<IProcessingInstruction>().Value[0].Should().Be("count");
            }
        }

        [Test]
        public void ShouldParseFSharpWithComplexishExpressionTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"complex-ish-expression.fs.html";
            using (var stream = this.Read(Fname))
            {
                var doc = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                doc.Attributes.Count().Should().Be(2);
                doc.Attributes.First().Key.Should().Be("user");
                doc.Attributes.First().Value.Should().Be("Base2art.NS.IUser");
                doc.Attributes.Skip(1).First().Key.Should().Be("count");
                doc.Attributes.Skip(1).First().Value.Should().Be("int");

                doc.ChildNodes.First().As<ITextNode>().Text.Should().Be("Some Text ");
                doc.ChildNodes.Skip(1)
                    .First()
                    .As<IProcessingInstruction>()
                    .Value[0].Should()
                    .Be("string.Join(\",\", user.Roles)");
                doc.ChildNodes.Skip(2).First().As<ITextNode>().Text.Should().Be(" ");
                doc.ChildNodes.Skip(3).First().As<IProcessingInstruction>().Value[0].Should().Be("count");
            }
        }

        [Test]
        public void ShouldParseFSharpWithCommentTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"comment.fs.html";
            using (var stream = this.Read(Fname))
            {
                var doc = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                doc.ChildNodes.Count().Should().Be(5);
                doc.ChildNodes.First().As<ITextNode>().Text.Should().Be("s\r\n");
                doc.ChildNodes.Skip(1)
                    .First()
                    .As<IComment>()
                    .Text.Should()
                    .Be("\r\nSome Text @{string.Join(\",\", user.Roles)} @{count}\r\n\r\n");
                doc.ChildNodes.Skip(2).First().As<ITextNode>().Text.Trim().Should().Be("e");
                doc.ChildNodes.Skip(3).First().As<IComment>().Text.Should().Be(" Comment 2 *");
                doc.ChildNodes.Skip(4).First().As<IComment>().Text.Should().Be(" Comment 3**  ");
            }
        }

        [Test]
        public void ShouldParseFSharpWithRawTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"raw.fs.html";
            using (var stream = this.Read(Fname))
            {
                var doc = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                doc.ChildNodes.Count().Should().Be(3);
                doc.ChildNodes.First().As<IProcessingInstruction>().Value[0].Should().Be("user.Name");
                doc.ChildNodes.First().As<IProcessingInstruction>().Raw.Should().Be(false);

                doc.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Value[0].Should().Be("user.Name");
                doc.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Raw.Should().Be(true);

                doc.ChildNodes.Skip(2).First().As<ITextNode>().Text.Should().Be("{cv}");

            }
        }

        [Test]
        public void ShouldParseFSharpWithParametersWithImportTemplate()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"parameters-with-import.fs.html";
            using (var stream = this.Read(Fname))
            {
                var doc = fsp.Lex(stream, this.Create(Fname)).ReturnValue;
                doc.Attributes.Count().Should().Be(2);
                doc.Attributes.First().Key.Should().Be("user");
                doc.Attributes.First().Value.Should().Be("Base2art.NS.IUser");
                doc.Attributes.Skip(1).First().Key.Should().Be("count");
                doc.Attributes.Skip(1).First().Value.Should().Be("int");

                doc.ChildNodes.First().As<ITextNode>().Text.Should().Be("# Some \r\n$ Text \r\n\r\n");
                doc.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Value[0].Should().Be("user.Name");
                doc.ChildNodes.Skip(2).First().As<ITextNode>().Text.Should().Be(" ");
                doc.ChildNodes.Skip(3).First().As<IProcessingInstruction>().Value[0].Should().Be("count");
            }
        }

        [Test]
        public void ShouldParseFSharpNestedFunction()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"nested-function.fs.html";
            using (var stream = this.Read(Fname))
            {
                var rex = fsp.Lex(stream, this.Create(Fname));
                
                rex.ResultCode.Should().Be(0);
                var doc = rex.ReturnValue;
                
                doc.Attributes.Count().Should().Be(2);
                doc.ChildNodes.Skip(0).First().As<IProcessingInstruction>().Value.First().Should().Be("people.map($func)");
                doc.ChildNodes.Skip(0).First().As<IProcessingInstruction>().Inner.Should().NotBeNull();
                doc.ChildNodes.Skip(0).First().As<IProcessingInstruction>().Inner.ChildNodes.First().As<ITextNode>().Text.Should().Be("<li /> ");
                
                doc.ChildNodes.Skip(1).First().As<ITextNode>().Text.Should().BeNullOrWhiteSpace();
                
                doc.ChildNodes.Skip(2).First().As<IProcessingInstruction>().Value.First().Should().Be("companies.map($func)");
                doc.ChildNodes.Skip(2).First().As<IProcessingInstruction>().Inner.Should().NotBeNull();
                doc.ChildNodes.Skip(2).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(0).First().As<ITextNode>().Text.Should().Be("<li>");
                doc.ChildNodes.Skip(2).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Value.First().Should().Be("$_.Name");
                doc.ChildNodes.Skip(2).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(2).First().As<ITextNode>().Text.Should().Be("</li> ");
                
                doc.ChildNodes.Skip(3).First().As<ITextNode>().Text.Should().BeNullOrWhiteSpace();
                
                doc.ChildNodes.Skip(4).First().As<IProcessingInstruction>().Value.First().Should().Be("companies.map($func)");
                doc.ChildNodes.Skip(4).First().As<IProcessingInstruction>().Inner.Should().NotBeNull();
                doc.ChildNodes.Skip(4).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(0).First().As<ITextNode>().Text.Should().Be("<li>\r\n        <ul>");
                doc.ChildNodes.Skip(4).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Value.First().Should().Be("$_.Employees.map($func)");
                doc.ChildNodes.Skip(4).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(0).First().As<ITextNode>().Text.Trim().Should().Be("<li>");
                doc.ChildNodes.Skip(4).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Value.First().Should().Be("$_.Name");
                doc.ChildNodes.Skip(4).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(2).First().As<ITextNode>().Text.Trim().Should().Be("</li>");
                doc.ChildNodes.Skip(4).First().As<IProcessingInstruction>().Inner.ChildNodes.Skip(2).First().As<ITextNode>().Text.Should().Be("\r\n        </ul>\r\n    </li>\r\n");
            }
        }

        [Test]
        public void ShouldParseFSharpExtensionFunction()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"extension-function.fs.html";
            using (var stream = this.Read(Fname))
            {
                var rex = fsp.Lex(stream, this.Create(Fname));
                
                rex.ResultCode.Should().Be(0);
                var doc = rex.ReturnValue;
                
                doc.Attributes.Count().Should().Be(2);
                doc.ChildNodes.Skip(0).First().As<IProcessingInstruction>().Value.First().Should().Be("this._if(true, $func)");
                doc.ChildNodes.Skip(0).First().As<IProcessingInstruction>().Inner.Should().NotBeNull();
                doc.ChildNodes.Skip(0).First().As<IProcessingInstruction>().Inner.ChildNodes.First().As<ITextNode>().Text.Should().Be("<li /> ");
                
                doc.ChildNodes.Skip(1).First().As<ITextNode>().Text.Should().BeNullOrWhiteSpace();
                
                doc.ChildNodes.Skip(2).First().As<IProcessingInstruction>().Value.First().Should().Be("this._if(true, $func)");
                doc.ChildNodes.Skip(2).First().As<IProcessingInstruction>().Inner.Should().NotBeNull();
                doc.ChildNodes.Skip(2).First().As<IProcessingInstruction>().Inner.ChildNodes.First().As<ITextNode>().Text.Should().Be("<li /> \r\n");
                
            }
        }

        [Test]
        public void ShouldParseFSharpNestedFunctionWithError()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"nested-function-with-error.fs.html";
            using (var stream = this.Read(Fname))
            {
                var doc = fsp.Lex(stream, this.Create(Fname)).ResultCode.Should().NotBe(0);
            }
        }

        [Test]
        public void ShouldParseFSharpWithBom()
        {
            var fsp = new FSharpTemplateParser();
            const string Fname = @"bom-test.fs.html";
            using (var stream = this.Read(Fname))
            {
                using (var sr = new StreamReader(stream))
                {
                    var text = sr.ReadToEnd();

                    using (var str = new MemoryStream())
                    {
                        using (var sw = new StreamWriter(str))
                        {
                            sw.Write(text);
                            sw.Flush();

                            str.Seek(0L, SeekOrigin.Begin);

                            var doc = fsp.Lex(str, this.Create(Fname)).ReturnValue;
                            doc.ChildNodes.Count().Should().Be(3);
                        }
                    }
                }
            }
        }

        private FileData Create(string fname)
        {
            return new FileData {
                CodeType = CodeTypes.FSharp,
                FileNameNoExtension = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(fname)),
                FileType = FileTypes.Html,
                Namespace = "",
                OriginalPath = "",
            };
        }

        private Stream Read(string fname)
        {
            return
                Assembly.GetExecutingAssembly()
                    .GetManifestResourceStream(
                "Base2art.MonkeyTail.Compiler.Features.Fixtures.SimpleTemplate." + fname);
        }
    }
}

