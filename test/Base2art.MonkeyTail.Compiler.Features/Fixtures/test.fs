namespace Views.Html.User

open Base2art.MonkeyTail
open Base2art.MonkeyTail.Api
open Base2art.MonkeyTail.ContentTypes

type Index() =

  inherit ExpressiveTemplateBase<Html, IFormat<Html>>(new HtmlFormat())

  interface ITemplate1<int, Html> with
    member this.Render(count:int) = this.Render(count)
  end

  with
    static member Apply(count:int) = (new Index()).Render(count)
    member this.Render(count:int) = 
         let Escape(text:System.Object) =
                                match text with
                                            | :? System.String as x -> this.Format.Escape(x)
                                            | :? Html as html -> html
                                            | line -> this.Format.Escape(line.ToString())
         let Raw(text:System.Object) =
                                match text with
                                            | :? System.String as x -> this.Format.Raw(x)
                                            | :? Html as html -> html
                                            | line -> this.Format.Raw(line.ToString())

         this.Format.Raw(this.Text0).Append(Escape(this.ProcInst1(count))).Append(this.Format.Raw(this.Text2))

    member this.Text0 = "\"Start\""
    member this.ProcInst1(count:int) = count * 2
    member this.Text2 = "End"
