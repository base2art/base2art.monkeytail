namespace Views.Html.User

open Base2art.MonkeyTail
open Base2art.MonkeyTail.Api
open Base2art.MonkeyTail.ContentTypes

type Index() =

  inherit ExpressiveTemplateBase<Html, IFormat<Html>>(new HtmlFormat())

  interface ITemplate1<int, Html> with
    member this.Render(count:int) = this.Render(count)
  end

  with
    static member Apply(count:int) = (new Index()).Render(count)
    member this.Render(count:int) = this.Format.Raw(this.Text0).Append(this.Format.Escape(this.ProcInst1(count).ToString())).Append(this.Format.Raw(this.Text2))

    member this.Text0 = "\"Start\""
    member this.ProcInst1(count:int) = count * 2
    member this.Text2 = "End"
