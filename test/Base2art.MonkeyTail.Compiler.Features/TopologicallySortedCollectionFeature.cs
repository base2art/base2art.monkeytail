
namespace Base2art.MonkeyTail.Compiler.Features
{
    using System;
    using System.Linq;
    using NUnit.Framework;
    using FluentAssertions;

    [TestFixture]
    public class TopologicallySortedCollectionFeature
    {
        [Test]
        public void ShouldSortSimply()
        {
            var collection = new DelegatedTopologicallySortedCollection<string, Person>(
                                 x => x.Name,
                                 x => x.Requires);
            
            collection.Add(new Person{ Name = "A" });
            collection.Add(new Person{ Name = "E", Requires = new []{ "D" } });
            collection.Add(new Person{ Name = "B", Requires = new []{ "A" } });
            collection.Add(new Person{ Name = "D", Requires = new []{ "C" } });
            collection.Add(new Person{ Name = "C", Requires = new []{ "B" } });
            
            var items = collection.ToArray();
            items[0].Name.Should().Be("A");
            items[1].Name.Should().Be("B");
            items[2].Name.Should().Be("C");
            items[3].Name.Should().Be("D");
            items[4].Name.Should().Be("E");
            
            collection.Remove(new Person {
                Name = "B",
                Requires = new []{ "A" }
            });
            
            items = collection.ToArray();
            items[0].Name.Should().Be("A");
//            items[1].Name.Should().Be("B");
            items[1].Name.Should().Be("C");
            items[2].Name.Should().Be("D");
            items[3].Name.Should().Be("E");
        }
        
        [Test]
        public void ShouldRecognizeCircularDependency()
        {
            var collection = new DelegatedTopologicallySortedCollection<string, Person>(
                                 x => x.Name,
                                 x => x.Requires);
            
            collection.Add(new Person{ Name = "A", Requires = new []{ "D" } });
            collection.Add(new Person{ Name = "B", Requires = new []{ "C" }  });
            collection.Add(new Person{ Name = "C", Requires = new []{ "D" }  });
            
            new Action(() => 
                        collection.Add(new Person {
                Name = "D",
                Requires = new []{ "A" }
            }))
                .ShouldThrow<InvalidOperationException>();
        }
        
        private class Person
        {
            public string Name { get; set; }
            public string[] Requires { get; set; }
        }
    }
}

