namespace Base2art.MonkeyTail.Compiler.Features
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using System.Reflection;
    using Base2art.MonkeyTail;
    using Base2art.MonkeyTail.Api;
    using Base2art.MonkeyTail.CodeDom;
    using Base2art.MonkeyTail.CodeDom.Impl;
    using Base2art.MonkeyTail.Config;

    using Base2art.MonkeyTail.ContentTypes;
    using Base2art.MonkeyTail.Diagnostics;
    using Base2art.MonkeyTail.LexerParser;
    using Base2art.MonkeyTail.LexerParser.Langs;
    using FluentAssertions;

    using NUnit.Framework;
    
    using Person = Models.Person;

    [TestFixture]
    public class ParserFeature
    {
        [Test]
        public void ShouldParseDirectory()
        {
            var enumerator = new TemplateEnumerator(DirectoryProvider.SimpleFileCompileDirectory);
            string outDir = DirectoryProvider.SimpleFileCompileOutputDirectory;
            Directory.CreateDirectory(outDir);
            var parser = new Parser(
                             CodeTypes.FSharp,
                             enumerator, 
                             this.Settings(outDir, "appViews.dll"),
                             null,
                             outDir);
            var rez = parser.Parse(new TextWriterLogger(Console.Out));
            
            foreach (var item in rez.Errors)
            {
                Console.WriteLine(item.Message);
            }
            
            rez.ResultCode.Should().Be(0);
            File.Exists(outDir + @"\views\html\index.fs").Should().BeTrue();
            File.Exists(outDir + @"\views\html\Edit.fs").Should().BeTrue();
        }
        
        [Test]
        public void ShouldParseDirectoryWithIncludes()
        {
            var enumerator = new TemplateEnumerator(DirectoryProvider.DependantTemplatesDirectory);
            string outDir = DirectoryProvider.DependantTemplatesOutputDirectory;
            Directory.CreateDirectory(outDir);
            
            var parser = new Parser(
                             CodeTypes.FSharp,
                             enumerator,
                             this.Settings(outDir, "appViewsDep.dll"),
                             null,
                             outDir);
            var rez = parser.Parse(new TextWriterLogger(Console.Out));
            foreach (var item in rez.Errors)
            {
                Console.WriteLine(item.Message);
            }
            
            rez.ResultCode.Should().Be(0);
        }
        
        [Test]
        public void ShouldParseDirectoryWithNestedTemplates()
        {
            var enumerator = new TemplateEnumerator(DirectoryProvider.NestedTemplatesDirectory);
            
            string outDir = DirectoryProvider.NestedTemplatesOutputDirectory;
            Directory.CreateDirectory(outDir);
            var settings = this.Settings(outDir, "appViewsNT.dll", new Reference
                {
                    Name = "Base2art.MonkeyTail.Compiler.Models",
                    HintPath = DirectoryProvider.GetHintPath("Base2art.MonkeyTail.Compiler.Models", true)
                }, new Reference
                {
                    Name = "Base2art.MonkeyTail.Api.Futures",
                    HintPath = DirectoryProvider.GetHintPath("Base2art.MonkeyTail.Api.Futures", false)
                });
            var parser = new Parser(
                             CodeTypes.FSharp,
                             enumerator,
                             settings,
                             null,
                             outDir);
            var rez = parser.Parse(new TextWriterLogger(Console.Out));
            foreach (var item in rez.Errors)
            {
                Console.WriteLine(item.Message);
            }
            
            rez.ResultCode.Should().Be(0);
            
            var asm = Assembly.LoadFile(settings.OutputFile);
            var t = Activator.CreateInstance(asm.GetType("Views.Html.Index")) as ITemplate1<IEnumerable<Person>, Html>;
            var rez1 = t.Render(new []
                {
                    new Person{ Name = "Scott Youngblut" },
                    new Person{ Name = "Matt Youngblut" }
                });
            rez1.Body.Should().NotContain("<h3>No People</h3>");
            rez1.Body.Should().Contain("<li>Scott Youngblut</li>");
            rez1.Body.Should().Contain("<li>Matt Youngblut</li>");
            
            rez1 = t.Render(new Person[]{ });
            rez1.Body.Should().Contain("<h3>No People</h3>");
            
            var t1 = Activator.CreateInstance(asm.GetType("Views.Html.Ext")) as ITemplate1<IEnumerable<Person>, Html>;
            var rez2 = t1.Render(new []
                {
                    new Person{ Name = "Scott Youngblut", Height = 63 },
                    new Person{ Name = "Matt Youngblut", Height = 61 }
                });
            rez2.Body.Should().NotContain("<h3>No People</h3>");
            rez2.Body.Should().Contain("<li>Scott Youngblut - 63</li>");
            rez2.Body.Should().Contain("<li>Matt Youngblut - 61</li>");
               
            rez2 = t1.Render(new Person[]{ });
            rez2.Body.Should().Contain("<h3>No People</h3>");
            
            var textTemplate = Activator.CreateInstance(asm.GetType("Views.Txt.Ext")) as ITemplate1<IEnumerable<Person>, Txt>;
            var textTemplateRezult = textTemplate.Render(new []
                {
                    new Person{ Name = "Scott Youngblut", Height = 63 },
                    new Person{ Name = "Matt Youngblut", Height = 61 }
                });
            textTemplateRezult.Body.Should().NotContain("<h3>No People</h3>");
            textTemplateRezult.Body.Should().Contain("<li>Scott Youngblut - 63</li>");
            textTemplateRezult.Body.Should().Contain("<li>Matt Youngblut - 61</li>");
               
            textTemplateRezult = textTemplate.Render(new Person[]{ });
            textTemplateRezult.Body.Should().Contain("<h3>No People</h3>");
        }

        [Test]
        public void ShouldGetCorrectDirectories()
        {
            var enumerator = new TemplateEnumerator(DirectoryProvider.SimpleTemplatesDirectory);
            var ns = enumerator.First(x =>
                string.Equals(x.Namespace, "user", StringComparison.CurrentCulture) &&
                         string.Equals(x.FileNameNoExtension, "index", StringComparison.CurrentCulture));
            ns.Should().NotBeNull();
        }
        
        [Test]
        public void ShouldLoadTokensForBodySingleStatement()
        {
            var content = @"
<html>
	<head>
		<title>Include Test</title>
	</head>
	<body>
	@{ 
                System.DateTime.UtcNow
     }
		@{Views.Html.Refer.Apply()}
	</body>
</html>

";
            using (var ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(content)))
            {
                var tokenizer = new BodyLexer(new BodyTokenizer(new Base2art.MonkeyTail.IO.StreamAdvancer(ms)));
                
                CodeDocument cd = new CodeDocument(string.Empty, "Index", FileTypes.Html);
                tokenizer.Lex(cd);
                
                cd.ChildNodes.Skip(1).First().As<IProcessingInstruction>().Value[0].Should().Be("System.DateTime.UtcNow");
            }
        }
    
        [Test]
        public void ShouldLoadTokensForBody()
        {
            var content = @"
<html>
	<head>
		<title>Include Test</title>
	</head>
	<body>
	@{ 

  let cylinderVolume radius length : float =
       // Define a local value pi. 
       let pi = 3.14159
       length * pi * radius * radius
  cylinderVolume 2.0 3.0
     }
		@{Views.Html.Refer.Apply()}
	</body>
</html>

";
            using (var ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(content)))
            {
                var tokenizer = new BodyLexer(new BodyTokenizer(new Base2art.MonkeyTail.IO.StreamAdvancer(ms)));
                
                CodeDocument cd = new CodeDocument(string.Empty, "Index", FileTypes.Html);
                tokenizer.Lex(cd);
                
                var pi = cd.ChildNodes.Skip(1).First().As<IProcessingInstruction>();
                pi.Value[0].Should().Be("  let cylinderVolume radius length : float =");
                pi.Value[1].Should().Be("       // Define a local value pi. ");
                pi.Value[2].Should().Be("       let pi = 3.14159");
                pi.Value[3].Should().Be("       length * pi * radius * radius");
                pi.Value[4].Should().Be("  cylinderVolume 2.0 3.0");
                
//                var result = new FSharpCodeTemplateAssembler(cd).RenderProcessingInstruction(pi, new int[]{});
//                result.Item1.Should().Be(@"
//        let cylinderVolume radius length : float =
//             // Define a local value pi. 
//             let pi = 3.14159
//             length * pi * radius * radius
//        cylinderVolume 2.0 3.0");
            }
        }
        
        [Test]
        public void ShouldFindPrefixCorrectly()
        {
            new[] { "abc", "abcd" }.FindCommonPrefix().Should().Be("abc");
            new[] { "abc", "a" }.FindCommonPrefix().Should().Be("a");
            new[] { "abcd", "abc" }.FindCommonPrefix().Should().Be("abc");
            new[] { "a", "abc" }.FindCommonPrefix().Should().Be("a");
            new[] { "abc", "abc" }.FindCommonPrefix().Should().Be("abc");
            new[] { "abc" }.FindCommonPrefix().Should().Be("abc");
            new string[] { }.FindCommonPrefix().Should().Be(string.Empty);
            ((string[])null).FindCommonPrefix().Should().Be(string.Empty);
            new[] { "abc", "abc" }.FindCommonPrefix().Should().Be("abc");
        }
        
        [Test]
        public void ShouldParseDirectoryForUserSamples()
        {
            var enumerator = new TemplateEnumerator(DirectoryProvider.UserDataDirectory);
            
            string outDir = DirectoryProvider.UserDataOutputDirectory;
            Directory.CreateDirectory(outDir);
            var settings = this.Settings(outDir, "appViewsUD.dll", new Reference
                {
                    Name = "Base2art.MonkeyTail.Api.Futures",
                    HintPath = DirectoryProvider.GetHintPath("Base2art.MonkeyTail.Api.Futures", false)
                });
            var parser = new Parser(
                             CodeTypes.FSharp,
                             enumerator,
                             settings,
                             null,
                             outDir);
            var rez = parser.Parse(new TextWriterLogger(Console.Out));
            foreach (var item in rez.Errors)
            {
                Console.WriteLine(item.Message);
            }
            
            rez.ResultCode.Should().Be(1);
        }

        private IViewsSettings Settings(string outDir, string dllName, params Reference[] refs)
        {
            var path = DirectoryProvider.TemplatesDllPath;
            List<Reference> primaryRefs = new List<Reference>();
            primaryRefs.Add(new Reference
                {
                    Name = "Base2art.MonkeyTail.Api",
                    HintPath = path
                });
            
            if (refs != null)
            {
                foreach (var @ref in refs)
                {
                    primaryRefs.Add(@ref);
                }
            }
            
            var settings = new ViewsSettings
            {
                SkipSecondaryOutputFile = true,
                LinkerPath = DirectoryProvider.Fsc,
                References = primaryRefs.ToArray(),
                OutputFile = Path.Combine(outDir, dllName),
            };
            
            return settings;
        }
    }
}

