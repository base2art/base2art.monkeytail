namespace Base2art.MonkeyTail.Compiler.Features
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Base2art.MonkeyTail.Config;

    public static class DirectoryProvider
    {
        private static readonly Lazy<string> TempDir = new Lazy<string>(WriteFiles);

        public static string SimpleFSharpFilesDir
        {
            get
            {
                return Path.Combine(TempDir.Value, "simple_f__files");
            }
        }

        public static string Fsc
        {
            get
            {
                return ViewsSettingsWrapper.Fsc;
            }
        }
        
        public static string SimpleTemplatesDirectory
        {
            get
            {
                return Path.Combine(TempDir.Value, "SimpleTemplate");
            }
        }
        
        public static string SimpleFileCompileDirectory
        {
            get
            {
                return Path.Combine(TempDir.Value, "SimpleFileCompile");
            }
        }
        
        public static string NestedTemplatesDirectory
        {
            get
            {
                return Path.Combine(TempDir.Value, "NestedTemplates");
            }
        }
        
        public static string UserDataDirectory
        {
            get
            {
                return Path.Combine(TempDir.Value, "UserData");
            }
        }
        
        public static string DependantTemplatesDirectory
        {
            get
            {
                return Path.Combine(TempDir.Value, "DependantTemplates");
            }
        }

        public static string SimpleTemplatesOutputDirectory
        {
            get
            {
                return Path.Combine(TempDir.Value, "output\\SimpleTemplate");
            }
        }

        public static string SimpleFileCompileOutputDirectory
        {
            get
            {
                return Path.Combine(TempDir.Value, "output\\SimpleFileCompile");
            }
        }

        public static string DependantTemplatesOutputDirectory
        {
            get
            {
                return Path.Combine(TempDir.Value, "output\\DependantTemplates");
            }
        }

        public static string NestedTemplatesOutputDirectory
        {
            get
            {
                return Path.Combine(TempDir.Value, "output\\NestedTemplates");
            }
        }

        public static string UserDataOutputDirectory
        {
            get
            {
                return Path.Combine(TempDir.Value, "output\\UserData");
            }
        }

        public static string TemporaryFSharpFile
        {
            get
            {
                return Path.Combine(TempDir.Value, "test.fs");
            }
        }

        public static string TemporaryFSharpFile2
        {
            get
            {
                return Path.Combine(TempDir.Value, "test2.fs");
            }
        }
        
        public static string GetHintPath(string dllName, bool isTest = false)
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var localPath = new Uri(codeBase).LocalPath;
            var di = new DirectoryInfo(localPath);
            //
            while (di != null && ( di.Name.ToLowerInvariant() != "base2art.monkeytail" && !File.Exists(Path.Combine(di.FullName, "Base2art.MonkeyTail.sln"))))
            {
                di = di.Parent;
            }

            #if DEBUG
                const string Mode = "Debug";
            #else
            const string Mode = "Release";
            #endif
            return Path.Combine(
                di.FullName,
                isTest ? "test": "src" ,
                dllName,
                "bin",
                Mode,
                dllName + ".dll");
        }

        public static string TemplatesDllPath
        {
            get
            {
                return GetHintPath("Base2art.MonkeyTail.Api");
            }
        }

        private static string WriteFiles()
        {
            const string Temp = @"C:\Temp\f#";
            if (Directory.Exists(Temp))
            {
                Directory.Delete(Temp, true);
            }

            Directory.CreateDirectory(Temp);

            Draw(Temp, new[] { "fs", "html" });
            Draw(Temp, new[] { "fs", "txt" });

            return Path.Combine(Temp, "Fixtures");
        }

		private static void Draw(string Temp, string[] exts)
		{
			var asm = Assembly.GetExecutingAssembly();
			var streams = asm.GetManifestResourceNames();
			foreach (var streamName in streams)
			{
				if (streamName.Contains(".Fixtures."))
				{
					var parts = streamName.Split('.').SkipWhile(x => x != "Fixtures").Reverse().SkipWhile(exts.Contains).Reverse();
					var ext = string.Join(".", streamName.Split('.').Reverse().TakeWhile(exts.Contains).Reverse());
					var end = string.Join("\\", parts) + "." + ext;
					var path = Path.Combine(Temp, end);
					Directory.CreateDirectory(Path.GetDirectoryName(path));
					using (var stream = asm.GetManifestResourceStream(streamName))
					{
						using (var sr = new StreamReader(stream))
						{
							File.WriteAllText(path, sr.ReadToEnd());
						}
					}
				}
			}
		}
    }
}

