namespace Base2art.MonkeyTail.Compiler.Features
{
    using System.IO;
    using System.Linq;

    using Base2art.MonkeyTail.IO;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class ExecutionFeature
    {
        [Test]
        public void ShouldRunProgram()
        {
            var rez = ProcessRunner.Execute("cmd", "/C \"echo 2\"", null);
            rez.ErrorOutput.Trim().Should().Be("");
            rez.StandardOutput.Trim().Should().Be("2");
            rez.ExitCode.Should().Be(0);
        }

        [Test]
        public void ShouldFSharpCompiler()
        {
            var dir = DirectoryProvider.SimpleFSharpFilesDir;
            var info = new DirectoryInfo(dir);

            var items = string.Join(" ", info.EnumerateFiles("*.fs").Select(x => x.FullName));

            var rez = ProcessRunner.Execute(
                    DirectoryProvider.Fsc,
                    @"--nologo --target:library --reference:" + DirectoryProvider.TemplatesDllPath +" " + items, null);
            rez.ExitCode.Should().Be(0, rez.ErrorOutput);
            rez.ErrorOutput.Trim().Should().Be("");
            rez.StandardOutput.Trim().Should().Be("");
        }
    }
}

