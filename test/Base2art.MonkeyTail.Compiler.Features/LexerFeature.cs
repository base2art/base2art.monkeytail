
namespace Base2art.MonkeyTail.Compiler.Features
{
    using System;
    using System.IO;
    using Base2art.MonkeyTail.LexerParser.Langs;
    using FluentAssertions;
    using NUnit.Framework;

    public class LexerFeature
    {
        [Test]
        public void ShouldLoadTokens()
        {
            var content = @"
#require System.Text
#require Null

#reference Base2art.NS
#reference Views.Html.Themes.ThemeName.Index

@count : int
@items:IDictionary<int, string>

@-

";
			
            using (var ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(content)))
            {
                var tokenizer = new HeaderLexer(new HeaderTokenizer(new Base2art.MonkeyTail.IO.StreamAdvancer(ms)));
                
                tokenizer.Requires().Should().BeEquivalentTo(new string[] {
                    "System.Text",
                    "Null"
                });
                
                tokenizer.References().Should().BeEquivalentTo(new string[] {
                    "Base2art.NS",
                    "Views.Html.Themes.ThemeName.Index"
                });
                
                var parameters = tokenizer.Parameters();
                parameters[0].Name.Should().Be("count");
                parameters[0].TypeName.Should().Be("int");
                parameters[1].Name.Should().Be("items");
                parameters[1].TypeName.Should().Be("IDictionary<int, string>");
            }
        }
    }
}

