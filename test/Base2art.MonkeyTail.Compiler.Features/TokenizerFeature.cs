
namespace Base2art.MonkeyTail.Compiler.Features
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
	using Base2art.MonkeyTail.CodeDom.Impl;
    using Base2art.MonkeyTail.LexerParser.Langs;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class TokenizerFeature
    {
        [Test]
        public void ShouldLoadTokens()
        {
            var content = @"
#require   {\t}    abc
#require Null

#reference Base2art.NS

@count : int
@items:IDictionary<int, string>

@-

";
            content = content.Replace("{\\t}", "\t");
            using (var ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(content)))
            {
                var tokenizer = new HeaderTokenizer(new Base2art.MonkeyTail.IO.StreamAdvancer(ms));
                var tokens = tokenizer.Parse().GetEnumerator();
                
                Verify(tokens, 0, HeaderTokenType.NewLine, "\r\n");
                Verify(tokens, 1, HeaderTokenType.Pound, "#");
                Verify(tokens, 2, HeaderTokenType.Word, "require");
                Verify(tokens, 3, HeaderTokenType.WhiteSpace, "   \t    ");
                Verify(tokens, 4, HeaderTokenType.Word, "abc");
                
                Verify(tokens, 5, HeaderTokenType.NewLine, "\r\n");
                Verify(tokens, 6, HeaderTokenType.Pound, "#");
                Verify(tokens, 7, HeaderTokenType.Word, "require");
                Verify(tokens, 8, HeaderTokenType.WhiteSpace, " ");
                Verify(tokens, 9, HeaderTokenType.Word, "Null");
                
                Verify(tokens, 10, HeaderTokenType.NewLine, "\r\n");
                Verify(tokens, 11, HeaderTokenType.NewLine, "\r\n");
                
                Verify(tokens, 12, HeaderTokenType.Pound, "#");
                Verify(tokens, 13, HeaderTokenType.Word, "reference");
                Verify(tokens, 14, HeaderTokenType.WhiteSpace, " ");
                Verify(tokens, 15, HeaderTokenType.Word, "Base2art");
                Verify(tokens, 16, HeaderTokenType.Dot, ".");
                Verify(tokens, 17, HeaderTokenType.Word, "NS");
                
                Verify(tokens, 18, HeaderTokenType.NewLine, "\r\n");
                Verify(tokens, 19, HeaderTokenType.NewLine, "\r\n");
                
                Verify(tokens, 20, HeaderTokenType.MonkeyTail, "@");
                Verify(tokens, 21, HeaderTokenType.Word, "count");
                Verify(tokens, 22, HeaderTokenType.WhiteSpace, " ");
                Verify(tokens, 23, HeaderTokenType.Colon, ":");
                Verify(tokens, 24, HeaderTokenType.WhiteSpace, " ");
                Verify(tokens, 25, HeaderTokenType.Word, "int");
                
                Verify(tokens, 26, HeaderTokenType.NewLine, "\r\n");
                Verify(tokens, 27, HeaderTokenType.MonkeyTail, "@");
                Verify(tokens, 28, HeaderTokenType.Word, "items");
                Verify(tokens, 29, HeaderTokenType.Colon, ":");
                Verify(tokens, 30, HeaderTokenType.Word, "IDictionary");
                Verify(tokens, 31, HeaderTokenType.LessThan, "<");
                Verify(tokens, 32, HeaderTokenType.Word, "int");
                Verify(tokens, 33, HeaderTokenType.Comma, ",");
                Verify(tokens, 34, HeaderTokenType.WhiteSpace, " ");
                Verify(tokens, 35, HeaderTokenType.Word, "string");
                Verify(tokens, 36, HeaderTokenType.GreaterThan, ">");
                
                Verify(tokens, 37, HeaderTokenType.NewLine, "\r\n");
                Verify(tokens, 38, HeaderTokenType.NewLine, "\r\n");
                
                Verify(tokens, 39, HeaderTokenType.EOF, "@-");
                tokens.MoveNext().Should().BeFalse();
            }
        }
        
        [Test]
        public void ShouldLoadTokensNoBodyMarker()
        {
            var content = @"
#require   {\t}    abc
#require Null

#reference Base2art.NS

@count : int

";
            content = content.Replace("{\\t}", "\t");
            using (var ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(content)))
            {
                var tokenizer = new HeaderTokenizer(new Base2art.MonkeyTail.IO.StreamAdvancer(ms));
                var tokens = tokenizer.Parse().GetEnumerator();
            }
        }
    
        [Test]
        public void ShouldToken()
        {
            var content = @"
@ -

@-@ -

";
            using (var ms = new MemoryStream(System.Text.Encoding.Default.GetBytes(content)))
            {
                var tokenizer = new HeaderTokenizer(new Base2art.MonkeyTail.IO.StreamAdvancer(ms));
                var tokens = tokenizer.Parse().GetEnumerator();
                
                Verify(tokens, 0, HeaderTokenType.NewLine, "\r\n");
                
                Verify(tokens, 1, HeaderTokenType.MonkeyTail, "@");
                Verify(tokens, 2, HeaderTokenType.WhiteSpace, " ");
                Verify(tokens, 3, HeaderTokenType.Dash, "-");
                
                Verify(tokens, 4, HeaderTokenType.NewLine, "\r\n");
                
                Verify(tokens, 5, HeaderTokenType.NewLine, "\r\n");
                
                Verify(tokens, 6, HeaderTokenType.EOF, "@-");
                
                tokens.MoveNext().Should().BeFalse();
            }
        }
    
        private void Verify(IEnumerator<IToken<HeaderTokenType>> tokens, int i, HeaderTokenType type, string str)
        {
            tokens.MoveNext();
            var token = tokens.Current;
            token.Type.Should().Be(type);
            token.Value.Should().Be(str);
        }
    }
}

