namespace Base2art.MonkeyTail.Features.Examples.Text
{
    using Base2art.MonkeyTail.Api;
    using Base2art.MonkeyTail.ContentTypes;

    public class RawRepeatText :
        ExpressiveTemplateBase<Txt, IFormat<Txt>>,
        ITemplate2<string, int, Txt>
    {
        public RawRepeatText()
            : base(new TxtFormat())
        {
        }

        public string Text0
        {
            get
            {
                return @"<html>
  <head>
    <title>Multiplier</title>
  </head>
  <body>
    <h2>Header</h2>
    <h3>";
            }
        }

        public string Text2
        {
            get
            {
                return @"</h3>
  <body>
</html>";
            }
        }

        public static Txt Apply(string value, int count)
        {
            return new RawRepeatText().Render(value, count);
        }

        public Txt Render(string value, int count)
        {
            return this.Format.Raw(this.Text0)
                .Append(this.Format.Raw(this.ProcInst1(value, count).ToString()))
                .Append(this.Format.Raw(this.Text2));
        }

        public string ProcInst1(string value, int count)
        {
            return value.Repeat(count);
        }

        Txt ITemplate2<string, int, Txt>.Render(string value, int count)
        {
            return this.Render(value, count);
        }
    }
}

