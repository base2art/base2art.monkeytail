namespace Base2art.MonkeyTail.Features.Examples.Xml
{
    using Base2art.MonkeyTail.Api;
    using Base2art.MonkeyTail.ContentTypes;

    public class Edit : 
        ExpressiveTemplateBase<Xml, IFormat<Xml>>,
        ITemplate1<int, Xml>
    {
        public Edit() : base(new XmlFormat())
        {
        }

        public string Text0
        {
            get
            {
                return @"<html>
  <head>
    <title>Multiplier</title>
  </head>
  <body>
    <h2>Header</h2>
    <h3>";
            }
        }

        public string Text2
        {
            get
            {
                return @"</h3>
  <body>
</html>";
            }
        }

        public static Xml Apply(int count)
        {
            return new Edit().Render(count);
        }

        public Xml Render(int count)
        {
            return this.Format.Raw(this.Text0)
                .Append(this.Format.Escape(this.ProcInst1(count).ToString()))
                .Append(this.Format.Raw(this.Text2));
        }

        public int ProcInst1(int count)
        {
            return count * 2;
        }

        Xml ITemplate1<int, Xml>.Render(int count)
        {
            return this.Render(count);
        }
    }
}

