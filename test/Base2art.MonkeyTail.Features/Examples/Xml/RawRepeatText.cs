namespace Base2art.MonkeyTail.Features.Examples.Xml
{
    using Base2art.MonkeyTail.Api;
    using Base2art.MonkeyTail.ContentTypes;

    public class RawRepeatText :
        ExpressiveTemplateBase<Xml, IFormat<Xml>>,
        ITemplate2<string, int, Xml>
    {
        public RawRepeatText()
            : base(new XmlFormat())
        {
        }

        public string Text0
        {
            get
            {
                return @"<html>
  <head>
    <title>Multiplier</title>
  </head>
  <body>
    <h2>Header</h2>
    <h3>";
            }
        }

        public string Text2
        {
            get
            {
                return @"</h3>
  <body>
</html>";
            }
        }

        public static Xml Apply(string value, int count)
        {
            return new RawRepeatText().Render(value, count);
        }

        public Xml Render(string value, int count)
        {
            return this.Format.Raw(this.Text0)
                .Append(this.Format.Raw(this.ProcInst1(value, count).ToString()))
                .Append(this.Format.Raw(this.Text2));
        }

        public string ProcInst1(string value, int count)
        {
            return value.Repeat(count);
        }

        Xml ITemplate2<string, int, Xml>.Render(string value, int count)
        {
            return this.Render(value, count);
        }
    }
}

