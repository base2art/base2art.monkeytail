namespace Base2art.MonkeyTail.Features.Examples.Javascript
{
    using Base2art.MonkeyTail.Api;
    using Base2art.MonkeyTail.ContentTypes;

    public class Edit :
        ExpressiveTemplateBase<Javascript, IFormat<Javascript>>,
        ITemplate1<int, Javascript>
    {
        public Edit()
            : base(new JavascriptFormat())
        {
        }

        public string Text0
        {
            get
            {
                return @"{ Value: ";
            }
        }

        public string Text2
        {
            get
            {
                return @" }";
            }
        }

        public static Javascript Apply(int count)
        {
            return new Edit().Render(count);
        }

        public Javascript Render(int count)
        {
            return this.Format.Raw(this.Text0)
                .Append(this.Format.Escape(this.ProcInst1(count).ToString()))
                .Append(this.Format.Raw(this.Text2));
        }

        public int ProcInst1(int count)
        {
            return count * 2;
        }

        Javascript ITemplate1<int, Javascript>.Render(int count)
        {
            return this.Render(count);
        }
    }
}

