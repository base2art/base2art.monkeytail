namespace Base2art.MonkeyTail.Features.Examples.Javascript
{
    using Base2art.MonkeyTail.Api;
    using Base2art.MonkeyTail.ContentTypes;

    public class RawRepeatText :
        ExpressiveTemplateBase<Javascript, IFormat<Javascript>>,
        ITemplate2<string, int, Javascript>
    {
        public RawRepeatText()
            : base(new JavascriptFormat())
        {
        }

        public string Text0
        {
            get
            {
                return @"{ Value: """;
            }
        }

        public string Text2
        {
            get
            {
                return @""" }";
            }
        }

        public static Javascript Apply(string value, int count)
        {
            return new RawRepeatText().Render(value, count);
        }

        public Javascript Render(string value, int count)
        {
            return this.Format.Raw(this.Text0)
                .Append(this.Format.Raw(this.ProcInst1(value, count).ToString()))
                .Append(this.Format.Raw(this.Text2));
        }

        public string ProcInst1(string value, int count)
        {
            return value.Repeat(count);
        }

        Javascript ITemplate2<string, int, Javascript>.Render(string value, int count)
        {
            return this.Render(value, count);
        }
    }
}

