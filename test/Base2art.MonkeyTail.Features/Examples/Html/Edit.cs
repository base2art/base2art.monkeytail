namespace Base2art.MonkeyTail.Features.Examples.Html
{
    using Base2art.MonkeyTail.Api;
    using Base2art.MonkeyTail.ContentTypes;

    public class Edit :
        ExpressiveTemplateBase<Html, IFormat<Html>>,
        ITemplate1<int, Html>
    {
        public Edit() : base(new HtmlFormat())
        {
        }

        public string Text0
        {
            get
            {
                return @"<html>
  <head>
    <title>Multiplier</title>
  </head>
  <body>
    <h2>Header</h2>
    <h3>";
            }
        }

        public string Text2
        {
            get
            {
                return @"</h3>
  <body>
</html>";
            }
        }

        public static Html Apply(int count)
        {
            return new Edit().Render(count);
        }

        public Html Render(int count)
        {
            return this.Format.Raw(this.Text0)
                .Append(this.Format.Escape(this.ProcInst1(count).ToString()))
                .Append(this.Format.Raw(this.Text2));
        }

        public int ProcInst1(int count)
        {
            return count * 2;
        }

        Html ITemplate1<int, Html>.Render(int count)
        {
            return this.Render(count);
        }
    }
}

