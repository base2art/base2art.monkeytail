namespace Base2art.MonkeyTail.Features.Examples.Html
{
    using Base2art.MonkeyTail;
    using Base2art.MonkeyTail.Api;
    using Base2art.MonkeyTail.ContentTypes;

    public class RepeatText :
        ExpressiveTemplateBase<Html, IFormat<Html>>,
        ITemplate2<string, int, Html>
    {
        public RepeatText()
            : base(new HtmlFormat())
        {
        }

        public string Text0
        {
            get
            {
                return @"<html>
  <head>
    <title>Multiplier</title>
  </head>
  <body>
    <h2>Header</h2>
    <h3>";
            }
        }

        public string Text2
        {
            get
            {
                return @"</h3>
  <body>
</html>";
            }
        }

        public static Html Apply(string value, int count)
        {
            return new RepeatText().Render(value, count);
        }

        public Html Render(string value, int count)
        {
            return this.Format.Raw(this.Text0)
                .Append(this.Format.Escape(this.ProcInst1(value, count).ToString()))
                .Append(this.Format.Raw(this.Text2));
        }

        public string ProcInst1(string value, int count)
        {
            return value.Repeat(count);
        }

        Html ITemplate2<string, int, Html>.Render(string value, int count)
        {
            return this.Render(value, count);
        }
    }
}

