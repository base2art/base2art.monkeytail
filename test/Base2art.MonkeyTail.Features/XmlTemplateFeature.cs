namespace Base2art.MonkeyTail.Features
{
    using Base2art.MonkeyTail.Features.Examples.Xml;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class XmlTemplateFeature
    {
        [Test]
        public void ShouldRunSimpleTemplate()
        {
            Edit.Apply(2).Body.Should().Contain("<h3>4</h3>");
            Edit.Apply(4).Body.Should().Contain("<h3>8</h3>");
        }

        [Test]
        public void ShouldRunEscapingHtmlTemplate()
        {
            RepeatText.Apply("<script>", 4).Body
                .Should().Contain("<h3>&lt;script&gt;&lt;script&gt;&lt;script&gt;&lt;script&gt;</h3>");

            RepeatText.Apply("<b>", 2).Body
                .Should().Contain("<h3>&lt;b&gt;&lt;b&gt;</h3>");

            RepeatText.Apply("<b>", 2).ContentType.Should().Be("text/xml");
        }

        [Test]
        public void ShouldRunRawHtmlTemplate()
        {
            RawRepeatText.Apply("<script>", 4).Body
                .Should().Contain("<h3><script><script><script><script></h3>");

            RawRepeatText.Apply("<b>", 2).Body
                .Should().Contain("<h3><b><b></h3>");

            RawRepeatText.Apply("<b>", 2).ContentType.Should().Be("text/xml");
        }
    }
}

