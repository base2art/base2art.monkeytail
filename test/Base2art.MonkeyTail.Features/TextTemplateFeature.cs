namespace Base2art.MonkeyTail.Features
{
    using Base2art.MonkeyTail.Features.Examples.Text;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class TextTemplateFeature
    {
        [Test]
        public void ShouldRunSimpleTemplate()
        {
            Edit.Apply(2).Body.Should().Contain("<h3>4</h3>");
            Edit.Apply(4).Body.Should().Contain("<h3>8</h3>");
        }

        [Test]
        public void ShouldRunEscapingTextTemplate()
        {
            RepeatText.Apply("<script>", 4).Body
                 .Should().Contain("<h3><script><script><script><script></h3>");

            RepeatText.Apply("<b>", 2).Body
                 .Should().Contain("<h3><b><b></h3>");

           RepeatText.Apply("<b>", 2).ContentType.Should().Be("text/plain");
        }

        [Test]
        public void ShouldRunRawTextTemplate()
        {
            RawRepeatText.Apply("<script>", 4).Body
                .Should().Contain("<h3><script><script><script><script></h3>");

            RawRepeatText.Apply("<b>", 2).Body
                .Should().Contain("<h3><b><b></h3>");

            RawRepeatText.Apply("<b>", 2).ContentType.Should().Be("text/plain");
        }
    }
}

