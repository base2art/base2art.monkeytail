namespace Base2art.MonkeyTail.Features
{
    using Base2art.MonkeyTail.Features.Examples.Javascript;

    using FluentAssertions;

    using NUnit.Framework;

    [TestFixture]
    public class JavascriptTemplateFeature
    {
        [Test]
        public void ShouldRunSimpleTemplate()
        {
            Edit.Apply(2).Body.Should().Contain("{ Value: 4 }");
            Edit.Apply(4).Body.Should().Contain("{ Value: 8 }");
        }

        [Test]
        public void ShouldRunEscapingTextTemplate()
        {
            RepeatText.Apply("\ntest\n", 4).Body
                 .Should().Be("{ Value: \"\\ntest\\n\\ntest\\n\\ntest\\n\\ntest\\n\" }");

            RepeatText.Apply("\ntest\n", 2).ContentType.Should().Be("text/javascript");
        }

        [Test]
        public void ShouldRunRawTextTemplate()
        {
            RawRepeatText.Apply("\ntest\n", 4).Body
                 .Should().Be("{ Value: \"\ntest\n\ntest\n\ntest\n\ntest\n\" }");

            RawRepeatText.Apply("\ntest\n", 2).ContentType.Should().Be("text/javascript");
        }
    }
}

